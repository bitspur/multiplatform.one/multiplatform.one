/*
 * File: /metro.config.js
 * Project: @platform/storybook-expo
 * File Created: 10-01-2025 21:02:36
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @type {import('expo/metro-config')}
 */

const { getDefaultConfig } = require("@expo/metro-config");
const path = require("node:path");
const { generate } = require("@storybook/react-native/scripts/generate");

generate({
  configPath: path.resolve(__dirname, "./.storybook"),
  useJs: true,
});

const projectRoot = __dirname;
const workspaceRoot = path.resolve(__dirname, "../..");
const config = getDefaultConfig(projectRoot);
config.resolver.resolverMainFields = [
  "sbmodern",
  "react-native",
  "module",
  "main",
];
config.resolver.sourceExts = [
  "storybook-expo.tsx",
  "storybook-expo.ts",
  "storybook-expo.js",
  "storybook-expo.jsx",
  "native.tsx",
  "native.ts",
  "native.js",
  "native.jsx",
  "tsx",
  "ts",
  "js",
  "jsx",
  ...(config.resolver.sourceExts || []),
];
config.watchFolders = [workspaceRoot];
config.transformer = config.transformer || {};
config.transformer.minifierPath = require.resolve("metro-minify-terser");
config.resolver.nodeModulesPaths = [
  path.resolve(projectRoot, "node_modules"),
  path.resolve(workspaceRoot, "node_modules"),
];
config.resolver.blockList = [
  /.*\/node_modules\/@vxrn\/.*/,
  /.*\/node_modules\/one\/.*/,
];
config.resolver.extraNodeModules = {
  ...config.resolver.extraNodeModules,
  "node:path": require.resolve("path-browserify"),
  "node:process": require.resolve("process/browser"),
  "node:url": require.resolve("url-polyfill"),
  "react-dom/server.browser": path.resolve(
    projectRoot,
    "node_modules/react-dom/server.browser.js",
  ),
  app: path.resolve(workspaceRoot, "app"),
  crypto: require.resolve("crypto-browserify"),
  fs: require.resolve("browserify-fs"),
  gql: path.resolve(workspaceRoot, "packages/gql"),
  one: require.resolve("path-browserify"),
  path: require.resolve("path-browserify"),
  process: require.resolve("process/browser"),
  stream: require.resolve("readable-stream"),
  ui: path.resolve(workspaceRoot, "packages/ui"),
  url: require.resolve("url-polyfill"),
  zlib: require.resolve("browserify-zlib"),
};
config.resetCache = true;
config.transformer.unstable_allowRequireContext = true;
module.exports = config;
