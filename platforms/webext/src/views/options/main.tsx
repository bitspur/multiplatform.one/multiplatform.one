/**
 * File: /src/views/options/main.tsx
 * Project: React WebExt
 * File Created: 10-01-2025 16:42:03
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import "../../../tamagui.css";
import "@tamagui/core/reset.css";
import { GlobalProvider } from "app/providers";
import tamaguiConfig from "app/tamagui.config";
import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { H1, Paragraph, YStack } from "ui";
import { ErrorBoundary } from "../../components/ErrorBoundary";

function Options() {
  return (
    <YStack width="100%" minHeight="100vh" padding="$4">
      <H1 size="$8" marginBottom="$4">
        React WebExt Options
      </H1>
      <Paragraph>This is the options page</Paragraph>
    </YStack>
  );
}

const container = document.getElementById("app");
if (container) {
  const root = createRoot(container);
  root.render(
    <StrictMode>
      <ErrorBoundary>
        <GlobalProvider tamaguiConfig={tamaguiConfig}>
          <Options />
        </GlobalProvider>
      </ErrorBoundary>
    </StrictMode>,
  );
}
