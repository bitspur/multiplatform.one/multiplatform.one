/**
 * File: /src/views/popup/main.tsx
 * Project: React WebExt
 * File Created: 10-01-2025 21:02:36
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import "../../../tamagui.css";
import "@tamagui/core/reset.css";
import { GlobalProvider } from "app/providers";
import tamaguiConfig from "app/tamagui.config";
import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { Button, H1, Paragraph, YStack } from "ui";
import browser from "webextension-polyfill";
import { ErrorBoundary } from "../../components/ErrorBoundary";

window.document.documentElement.style.width = "400px";
window.document.documentElement.style.height = "400px";

function Popup() {
  const openOptions = () => {
    browser.runtime.openOptionsPage();
  };

  const openSidePanel = async () => {
    const [tab] = await browser.tabs.query({
      active: true,
      currentWindow: true,
    });
    if (tab?.id) {
      await browser.sidePanel.open({ tabId: tab.id });
      window.close();
    }
  };

  return (
    <YStack width={400} minHeight={400} padding="$4">
      <H1>React WebExt</H1>
      <Paragraph>This is the popup page</Paragraph>
      <YStack space="$3" marginTop="$4">
        <Button onPress={openSidePanel}>Open Sidepanel</Button>
        <Button onPress={openOptions}>Open Options</Button>
      </YStack>
    </YStack>
  );
}

const container = document.getElementById("app");
if (container) {
  const root = createRoot(container);
  root.render(
    <StrictMode>
      <ErrorBoundary>
        <GlobalProvider tamaguiConfig={tamaguiConfig}>
          <Popup />
        </GlobalProvider>
      </ErrorBoundary>
    </StrictMode>,
  );
}
