/**
 * File: /src/background/contentScriptHMR.ts
 * Project: React WebExt
 * File Created: 10-01-2025 21:02:36
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { isFirefoxExtension } from "multiplatform.one";
import browser from "webextension-polyfill";
import { isForbiddenUrl } from "../../env";

const logger = console;

browser.webNavigation.onCommitted.addListener(({ tabId, frameId, url }) => {
  if (frameId !== 0) return;
  if (isForbiddenUrl(url)) return;
  browser.tabs
    .executeScript(tabId, {
      file: `${isFirefoxExtension ? "" : "."}/dist/contentScripts/index.global.js`,
      runAt: "document_end",
    })
    .catch(logger.error);
});
