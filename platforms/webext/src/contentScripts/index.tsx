/**
 * File: /src/contentScripts/index.tsx
 * Project: React WebExt
 * File Created: 10-01-2025 15:00:37
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import "../../tamagui.content.css";
import "@tamagui/core/reset.css";
import { ErrorBoundary } from "@multiplatform.one/components";
import tamaguiConfig from "app/tamagui.config";
import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { TamaguiProvider } from "tamagui";
import { Text } from "ui";

function ContentApp() {
  return (
    <div
      style={{
        position: "fixed",
        bottom: 16,
        right: 16,
        padding: 16,
        backgroundColor: "white",
        borderRadius: 8,
        boxShadow: "0 2px 4px rgba(0,0,0,0.1)",
        zIndex: 2147483647,
      }}
    >
      <Text fontWeight="bold" fontSize={18}>
        React WebExt
      </Text>
      <Text>Content script is running</Text>
    </div>
  );
}

(() => {
  const logger = console;
  logger.info("[@platform/webext] injected");
  function init() {
    try {
      const container = document.createElement("div");
      const root = document.createElement("div");
      const shadowDOM = container.attachShadow?.({ mode: "open" }) || container;
      shadowDOM.appendChild(root);
      document.body.appendChild(container);
      createRoot(root).render(
        <StrictMode>
          <ErrorBoundary>
            <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
              <ContentApp />
            </TamaguiProvider>
          </ErrorBoundary>
        </StrictMode>,
      );
    } catch (err) {
      logger.error("[@platform/webext]", err);
    }
  }
  if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", init);
  } else {
    init();
  }
})();
