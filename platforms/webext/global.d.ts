/*
 * File: /global.d.ts
 * Project: React WebExt
 * File Created: 10-01-2025 21:02:36
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { ProtocolWithReturn } from "webext-bridge";
import type { Browser as BaseBrowser } from "webextension-polyfill";

declare module "webext-bridge" {
  export interface ProtocolMap {
    "tab-prev": { title: string | undefined };
    "get-current-tab": ProtocolWithReturn<
      { tabId: number },
      { title?: string }
    >;
  }
}

declare module "webextension-polyfill" {
  interface SidePanelAPI {
    open: (options?: { tabId?: number }) => Promise<void>;
  }

  export interface Browser extends BaseBrowser {
    sidePanel: SidePanelAPI;
  }

  const browser: Browser;
  export default browser;
}

declare module "*.svg" {
  const content: string;
  export default content;
}

declare module "*.png" {
  const content: string;
  export default content;
}

declare module "*.jpg" {
  const content: string;
  export default content;
}

declare const __DEV__: boolean;
declare const __NAME__: string;
