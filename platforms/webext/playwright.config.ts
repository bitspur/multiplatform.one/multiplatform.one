/**
 * File: /playwright.config.ts
 * Project: React WebExt
 * File Created: 10-01-2025 21:02:36
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import path from "node:path";
import { defineConfig } from "@playwright/test";
import { isHeadless } from "./env";

const extensionPath = path.join(__dirname, "extension");

export default defineConfig({
  testDir: "./tests",
  fullyParallel: true,
  forbidOnly:
    !!process.env.CI && process.env.CI !== "false" && process.env.CI !== "0",
  retries:
    process.env.CI && process.env.CI !== "false" && process.env.CI !== "0"
      ? 2
      : 0,
  workers:
    process.env.CI && process.env.CI !== "false" && process.env.CI !== "0"
      ? 1
      : undefined,
  reporter: "html",
  use: {
    trace: "on-first-retry",
    screenshot: "on",
    headless: isHeadless,
  },
  projects: [
    {
      name: "chromium-with-extension",
      use: {
        browserName: "chromium",
        launchOptions: {
          args: [
            `--disable-extensions-except=${extensionPath}`,
            `--load-extension=${extensionPath}`,
            "--no-sandbox",
            "--headless=new",
          ],
          ignoreDefaultArgs: [
            "--disable-component-extensions-with-background-pages",
            "--disable-extensions",
          ],
          executablePath: process.env.PLAYWRIGHT_CHROMIUM_EXECUTABLE_PATH,
        },
      },
      testMatch: /.*\.spec\.ts/,
    },
  ],
});
