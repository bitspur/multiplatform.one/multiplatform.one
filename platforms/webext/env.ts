/**
 * File: /env.ts
 * Project: React WebExt
 * File Created: 10-01-2025 21:02:36
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const forbiddenProtocols = [
  "chrome-extension://",
  "chrome-search://",
  "chrome://",
  "devtools://",
  "edge://",
  "https://chrome.google.com/webstore",
];

export function isForbiddenUrl(url: string) {
  return forbiddenProtocols.some((protocol) => url.startsWith(protocol));
}
export const isDev = process.env.NODE_ENV !== "production";
export const port = Number(process.env.PORT) || 3303;
export const isHeadless = !!(
  (process.env.CI &&
    process.env.CI.toLowerCase() !== "false" &&
    process.env.CI !== "0") ||
  (process.env.REMOTE_CONTAINERS && !process.env.DISPLAY)
);
