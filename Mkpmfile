# File: /Mkpmfile
# Project: root
# File Created: 04-04-2024 15:50:39
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2021 - 2024
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include $(MKPM)/mkpm
include $(MKPM)/chain
include $(MKPM)/dotenv

PACKAGES := $(shell $(LS) packages)
PLATFORMS := $(shell $(LS) platforms)
DOCKER ?= docker

ACTIONS += deps
$(ACTION)/deps: package.json \
	api/package.json \
	app/package.json \
	ethereum/package.json \
	frappe/package.json \
	solana/package.json \
	sui/package.json \
	$(patsubst %,packages/%/package.json,$(PACKAGES)) \
	$(patsubst %,platforms/%/package.json,$(PLATFORMS))
	@$(RM) -rf package-lock.json yarn.lock
	@$(PNPM) install
	@$(call done,$@)

.PHONY: $(patsubst %,%/%,$(PLATFORMS))
$(patsubst %,%/%,$(PLATFORMS)):
	@$(call make,platforms/$(@D)) $*

.PHONY: $(patsubst %,%/%,$(PACKAGES))
$(patsubst %,%/%,$(PACKAGES)):
	@$(call make,packages/$(@D)) $*

.PHONY: update
update:
	@$(PNPM) update -ri

.PHONY: clean
clean:
	-@$(MKCACHE_CLEAN)
	-@$(WATCHMAN) watch-del-all $(NOFAIL)
	-@$(GIT) clean -fxd \
		$(MKPM_GIT_CLEAN_FLAGS)

.PHONY: purge
ifneq (/home/frappe,$(HOME))
purge: docker/down clean
else
purge: clean
endif
	-@rm -rf frappe/frappe-bench
	-@$(GIT) clean -fxd

.PHONY: storybook/docker/%
storybook/docker/%:
	@$(call make,platforms/storybook/docker)  $*

.PHONY: docker/%
docker/%: docker/data/logs/app.log
	@$(call make,docker) $*
docker/data/logs/app.log:
	@$(MKDIR) -p $(@D)
	@$(TOUCH) $@

.PHONY: api/%
api/%: force
	@$(call make,api) $*

.PHONY: ethereum/%
ethereum/%: force
	@$(call make,ethereum) $*

.PHONY: frappe/%
frappe/%: force
	@$(call make,frappe) $*

.PHONY: solana/%
solana/%: force
	@$(call make,solana) $*

.PHONY: sui/%
sui/%: force
	@$(call make,sui) $*

.PHONY: version
version:
	@$(ECHO) "---\n$$($(PNPM) list -r --json | $(JQ) -r '.[].path' | while read p; do \
		_NAME=$$($(CAT) "$$p/package.json" | $(JQ) -r '.name'); \
		if [ `$(CAT) "$$p/package.json" | $(JQ) -r '.private // false'` = "false" ]; then \
			echo "\"$${_NAME}\": patch"; \
		fi \
	done)\n---" > $(PROJECT_ROOT)/.changeset/patch.md
	@$(CHANGESET) version \
		--ignore "@platform/electron" \
		--ignore "@platform/keycloak" \
		--ignore "@platform/one" \
		--ignore "@platform/storybook" \
		--ignore "@platform/storybook-expo" \
		--ignore "@platform/vocs" \
		--ignore "@platform/vscode" \
		--ignore "@platform/webext"

.PHONY: publish
publish: version check
	@$(CHANGESET) publish

.PHONY: doctor
doctor:
	@$(GIT) clean -fxd -e .mkpm
	@$(RM) -rf $(HOME)/.local/state/frappe $(NOFAIL)
	@$(RM) -rf frappe/frappe-bench $(NOFAIL)
	@$(DOCKER) pull $(DOCKER_REGISTRY)/devcontainer:latest

.PHONY: spellcheck-prune
spellcheck-prune:
	-@$(MV) cspell.json _cspell.json $(NOFAIL)
	@M="$$($(PNPM) cspell --words-only --unique --no-progress --no-summary \
		`($(GIT) ls-files && ($(GIT) lfs ls-files | $(CUT) -d' ' -f3)) | $(SORT) | $(UNIQ) -u | \
		$(GREP) -v project-words.txt` 2>&1 | $(AWK) '{if ($$0 ~ /^[\x00-\x7F]*$$/) print $$0}' | $(TR) '[:upper:]' '[:lower:]' | $(SORT))" && \
		D="$$($(CAT) project-words.txt)" && \
		(echo "$$M" && echo "$$D") | $(SORT) | $(UNIQ) -d | $(TEE) _project-words.txt
	@$(MV) _cspell.json cspell.json
	@$(MV) _project-words.txt project-words.txt

.PHONY: down
down:
	@$(DOCKER) rm -f $$($(DOCKER) ps --filter "name=devcontainer" -a -q) $(NOFAIL)
	@for v in $$($(DOCKER) volume ls --filter "name=devcontainer" -q); do \
		$(DOCKER) rm -f $$($(DOCKER) ps -a -q --filter volume=$$v) $(NOFAIL); \
		$(DOCKER) volume rm -f $$v || true; \
	done
	@$(DOCKER) rm -f $$($(DOCKER) ps -a -q --filter network=$$($(DOCKER) network ls --filter "name=devcontainer" -q)) $(NOFAIL)
	@$(DOCKER) network ls --filter "name=devcontainer" -q | $(XARGS) -r $(DOCKER) network rm || $(TRUE)

.PHONY: stop
stop:
	@_CONTAINERS=$$($(DOCKER) ps --filter "name=devcontainer" -q); \
		if [ ! -z "$$_CONTAINERS" ]; then $(DOCKER) stop $$_CONTAINERS; fi

ACTIONS += build~deps
$(ACTION)/build: $(call git_deps,\.([mc]?[jt]sx?)$$)
	@$(PNPM) run build $(ARGS)
	@$(call done,$@)

ACTIONS += check~build
$(ACTION)/check: $(call git_deps,\.([mc]?[jt]sx?)$$)
	@$(PNPM) run check $(ARGS)
	@$(call done,$@)
.PHONY: check/%
check/%: ~build force
	@$(PNPM) run check $* $(ARGS)

ACTIONS += test~check
$(ACTION)/test: $(call git_deps,\.([mc]?[jt]sx?)$$)
	@$(PNPM) run test $(ARGS)
	@$(call done,$@)
.PHONY: test/%
test/%: ~check force
	@$(PNPM) run test $* $(ARGS)

SCRIPTS := count generate
.PHONY: $(SCRIPTS)
$(SCRIPTS): ~deps force
	@$(PNPM) run $@ $(ARGS)

-include $(call chain)
