use anchor_lang::prelude::*;

declare_id!("6z68wfurCMYkZG51s1Et9BJEd9nJGUusjHXNt4dGbNNF");

#[program]
pub mod basic {
    use super::*;

    pub fn initialize(ctx: Context<Initialize>, unlock_time: i64, amount: u64) -> Result<()> {
        require!(
            Clock::get()?.unix_timestamp < unlock_time,
            LockError::UnlockTimeNotInFuture
        );
        let lock = &mut ctx.accounts.lock;
        lock.unlock_time = unlock_time;
        lock.owner = ctx.accounts.authority.key();
        anchor_lang::system_program::transfer(
            CpiContext::new(
                ctx.accounts.system_program.to_account_info(),
                anchor_lang::system_program::Transfer {
                    to: ctx.accounts.vault.to_account_info(),
                    from: ctx.accounts.authority.to_account_info(),
                },
            ),
            amount,
        )?;
        Ok(())
    }

    pub fn withdraw(ctx: Context<Withdraw>) -> Result<()> {
        let lock = &ctx.accounts.lock;
        require!(
            Clock::get()?.unix_timestamp >= lock.unlock_time,
            LockError::TooEarlyToWithdraw
        );
        require!(
            ctx.accounts.owner.key() == lock.owner,
            LockError::UnauthorizedOwner
        );
        anchor_lang::system_program::transfer(
            CpiContext::new(
                ctx.accounts.system_program.to_account_info(),
                anchor_lang::system_program::Transfer {
                    to: ctx.accounts.owner.to_account_info(),
                    from: ctx.accounts.vault.to_account_info(),
                },
            ),
            ctx.accounts.vault.lamports(),
        )?;
        Ok(())
    }
}

#[derive(Accounts)]
pub struct Initialize<'info> {
    #[account(
        init,
        payer = authority,
        space = 8 + Lock::INIT_SPACE,
        seeds = [b"lock"],
        bump
    )]
    pub lock: Account<'info, Lock>,
    /// CHECK: This is safe because we only use it to transfer SOL
    #[account(mut)]
    pub vault: AccountInfo<'info>,
    #[account(mut)]
    pub authority: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
pub struct Withdraw<'info> {
    #[account(
        seeds = [b"lock"],
        bump,
    )]
    pub lock: Account<'info, Lock>,
    /// CHECK: This is safe because we only use it to transfer SOL
    #[account(mut)]
    pub vault: AccountInfo<'info>,
    #[account(mut)]
    pub owner: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[account]
#[derive(InitSpace)]
pub struct Lock {
    pub unlock_time: i64,
    pub owner: Pubkey,
}

#[error_code]
pub enum LockError {
    #[msg("Unlock time must be in the future")]
    UnlockTimeNotInFuture,

    #[msg("Cannot withdraw before unlock time")]
    TooEarlyToWithdraw,

    #[msg("Only the owner can withdraw")]
    UnauthorizedOwner,
}
