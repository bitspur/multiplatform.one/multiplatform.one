-- CreateEnum
CREATE TYPE "GameRoomStatus" AS ENUM ('WAITING_FOR_PLAYERS', 'IN_PROGRESS', 'FINISHED');

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "gameRoomId" TEXT;

-- CreateTable
CREATE TABLE "GameRoom" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "room_code" TEXT NOT NULL,
    "time_limit" INTEGER NOT NULL,
    "room_name" TEXT NOT NULL,
    "status" "GameRoomStatus" NOT NULL DEFAULT 'WAITING_FOR_PLAYERS',

    CONSTRAINT "GameRoom_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "User" ADD CONSTRAINT "User_gameRoomId_fkey" FOREIGN KEY ("gameRoomId") REFERENCES "GameRoom"("id") ON DELETE SET NULL ON UPDATE CASCADE;
