/*
  Warnings:

  - Added the required column `gameRoomId` to the `Guess` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Guess" ADD COLUMN     "gameRoomId" TEXT NOT NULL;

-- AddForeignKey
ALTER TABLE "Guess" ADD CONSTRAINT "Guess_gameRoomId_fkey" FOREIGN KEY ("gameRoomId") REFERENCES "GameRoom"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
