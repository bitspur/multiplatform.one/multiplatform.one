/**
 * File: /components/GeneralQuestions/index.tsx
 * Project: app
 * File Created: 18-01-2025 10:18:16
 *
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { useState } from "react";
import { Button, H2, Input, Text, YStack } from "ui";

export const GeneralQuestion = ({
  setShot,
}: {
  setShot: (show: boolean) => void;
}) => {
  const [answer, setAnswer] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const handleSubmit = () => {
    if (answer.trim() === "") {
      setErrorMessage("Please enter at least one character");
    } else {
      setAnswer("");
      setErrorMessage("");
      setShot(true);
    }
  };

  return (
    <YStack
      flex={1}
      alignItems="center"
      jc="center"
      gap="$4"
      w="100%"
      maxWidth={800}
      minWidth={320}
      $sm={{
        jc: "center",
        ai: "center",
        w: "100%",
      }}
    >
      <H2
        fontSize={54}
        $sm={{
          fontSize: 30,
          letterSpacing: 0.5,
          gap: "$1",
        }}
        textAlign="center"
        w="100%"
      >
        What is your new year resolution?
      </H2>

      <Input
        placeholder="Enter Your Answer"
        h="$5"
        value={answer}
        marginTop={20}
        w="90%"
        maxWidth={500}
        onChangeText={(text) => setAnswer(text)}
      />

      {errorMessage && <Text color="red">{errorMessage}</Text>}

      <Button size="$4" onPress={handleSubmit} borderRadius="$2">
        Submit Answer
      </Button>
    </YStack>
  );
};
