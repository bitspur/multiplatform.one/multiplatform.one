/**
 * File: /components/FAQAccordion.tsx
 * Project: app
 * File Created: 10-01-2025 14:33:06
 * Author: ffx
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Plus } from "@tamagui/lucide-icons";
import { Accordion, Paragraph, Square, YStack } from "ui";

const faqData = [
  {
    id: "a1",
    question: "What is your name?",
    answer: "My name is John Doe",
  },
  {
    id: "a2",
    question: "What is your favorite color?",
    answer: "My favorite color is blue",
  },
];

export const FAQAccordion = () => {
  return (
    <YStack ai="center" jc="center" paddingTop="$4" width="100%" maxWidth={600}>
      <Accordion
        overflow="hidden"
        width="100%"
        type="single"
        collapsible
        borderWidth={0}
      >
        {faqData.map((item) => (
          <Accordion.Item
            key={item.id}
            value={item.id}
            marginBottom="$4"
            bg="$color3"
            borderRadius="$4"
            borderWidth={1}
            borderColor="$color5"
          >
            <Accordion.Trigger
              flexDirection="row"
              justifyContent="space-between"
              padding="$3"
            >
              {({ open }: { open: boolean }) => (
                <>
                  <Paragraph>{item.question}</Paragraph>
                  <Square animation="quick" rotate={open ? "45deg" : "0deg"}>
                    <Plus size="$1" />
                  </Square>
                </>
              )}
            </Accordion.Trigger>

            <Accordion.HeightAnimator animation="quick">
              <Accordion.Content
                animation="quick"
                enterStyle={{ opacity: 0, y: -10 }}
                exitStyle={{ opacity: 0, y: -10 }}
                bg="$color2"
                padding="$3"
                borderTopWidth={1}
                borderColor="$color5"
                borderRadius="$4"
              >
                <Paragraph>{item.answer}</Paragraph>
              </Accordion.Content>
            </Accordion.HeightAnimator>
          </Accordion.Item>
        ))}
      </Accordion>
    </YStack>
  );
};
