/**
 * File: /components/CreateQuestion/index.tsx
 * Project: app
 * File Created: 07-02-2025 10:22:03
 *
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { useState } from "react";
import { Button, Card, Input, RadioGroup, Text, XStack, YStack } from "ui";

interface Question {
  text: string;
  time: number;
}

export const CreateQuestion = () => {
  const [question, setQuestion] = useState("");
  const [time, setTime] = useState(30); // Default to 30 seconds
  const [questions, setQuestions] = useState<Question[]>([]);
  const handleAddQuestion = () => {
    if (question.trim() && time) {
      setQuestions([...questions, { text: question, time }]);
      setQuestion("");
      // Don't reset time - keep the last selected value
    }
  };
  const handleSubmit = () => {
    console.log(questions, "questions");
  };

  console.log(time, "time");
  console.log(question, "question");
  return (
    <YStack fullscreen ai="center" jc="center">
      <Card
        ai="center"
        jc="center"
        gap="$4"
        p="$4"
        width="100%"
        maxWidth={500}
        minWidth={300}
      >
        <Text fontWeight="bold" fontSize="$8" $sm={{ fontSize: "$4" }}>
          Create Question
        </Text>
        <Input
          placeholder="write question here ..."
          value={question}
          w="90%"
          onChangeText={(text) => setQuestion(text)}
        />
        <Text fontWeight="bold">Select Time (seconds)</Text>
        <RadioGroup
          defaultValue="30"
          name="time"
          onValueChange={(value) => setTime(Number(value))}
        >
          <XStack gap="$4">
            <XStack ai="center" gap="$2">
              <RadioGroup.Item value="30" id="30">
                <RadioGroup.Indicator />
              </RadioGroup.Item>
              <Text htmlFor="30">30</Text>
            </XStack>
            <XStack ai="center" gap="$2">
              <RadioGroup.Item value="60" id="60">
                <RadioGroup.Indicator />
              </RadioGroup.Item>
              <Text htmlFor="60">60</Text>
            </XStack>
          </XStack>
        </RadioGroup>
        <XStack gap="$4">
          <Button onPress={handleAddQuestion}>Add Question</Button>
          <Button onPress={handleSubmit}>Submit</Button>
        </XStack>

        {questions.length > 0 && (
          <Card w="100%" gap="$4">
            <RadioGroup
              defaultValue={questions[0].text}
              name="questionSelection"
            >
              <YStack gap="$3">
                {questions.map((q, index) => (
                  <XStack
                    key={index}
                    ai="center"
                    gap="$2"
                    p="$2"
                    borderWidth={1}
                    borderColor="$color10"
                    marginTop="$2"
                  >
                    <RadioGroup.Item value={q.text} id={`question-${index}`}>
                      <RadioGroup.Indicator />
                    </RadioGroup.Item>
                    <Text htmlFor={`question-${index}`}>{q.text}</Text>
                    <Text color="$color10">({q.time} seconds)</Text>
                  </XStack>
                ))}
              </YStack>
            </RadioGroup>
          </Card>
        )}
      </Card>
    </YStack>
  );
};
