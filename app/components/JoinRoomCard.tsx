/**
 * File: /components/JoinRoomCard.tsx
 * Project: app
 * File Created: 16-01-2025 11:57:53
 * Author: ffx
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { useRouter } from "one";
import { useState } from "react";
import { Button, H1, Input, Label, Text, YStack } from "tamagui";

export const JoinRoomCard = () => {
  const [formData, setFormData] = useState({
    roomCode: "",
  });
  const [errors, setErrors] = useState({
    roomCode: "",
  });
  const router = useRouter();

  const validateRoomCode = (code: string) => {
    if (!code) {
      return "Room code is required";
    }
    if (code.length !== 6) {
      return "Room code must be 6 characters";
    }
    return "";
  };

  const handleChange = (value: string) => {
    setFormData((prev) => ({
      ...prev,
      roomCode: value,
    }));
    setErrors((prev) => ({
      ...prev,
      roomCode: "",
    }));
  };

  const handleSubmit = () => {
    const roomCodeError = validateRoomCode(formData.roomCode);

    setErrors({
      roomCode: roomCodeError,
    });

    if (!roomCodeError) {
      router.push(`/room/${formData.roomCode}`);
    }
  };

  return (
    <YStack justifyContent="center" alignItems="center" fullscreen w="100%">
      <YStack gap="$2" width="90%" maxWidth={500} minWidth={300} padding="$3">
        <YStack gap="$2">
          <H1 fontSize={50} $sm={{ fontSize: 30 }}>
            Join Room
          </H1>
          <Text fontSize={25} $sm={{ fontSize: 20 }}>
            Enter the room code and join the room
          </Text>
        </YStack>

        <Label>
          Room Code <Text color="$red10">*</Text>
        </Label>
        <Input
          placeholder="Enter Room Code.."
          value={formData.roomCode}
          onChangeText={(text) => handleChange(text)}
          maxLength={6}
          borderRadius="$2"
        />
        {errors.roomCode && (
          <Text color="$red10" fontSize={18} $sm={{ fontSize: 16 }}>
            {errors.roomCode}
          </Text>
        )}

        <Button
          marginTop={20}
          onPress={handleSubmit}
          disabled={!formData.roomCode}
        >
          Join Room
        </Button>
      </YStack>
    </YStack>
  );
};
