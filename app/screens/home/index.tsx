/**
 * File: /screens/home/index.tsx
 * Project: app
 * File Created: 07-08-2024 11:44:41
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { withAuthenticated } from "@multiplatform.one/keycloak";
import { useGqlQuery } from "@multiplatform.one/react-query-urql";
import MeetingIcon from "app/assets/meeting-room.svg";
import PlusIcon from "app/assets/plus-con.svg";
import WhoAmI from "app/assets/who-am-i.svg";
import { CreateQuestion } from "app/components/CreateQuestion/index";
import { FAQAccordion } from "app/components/FAQAccordion/index";
import { gql } from "gql";
import { useRouter } from "one";
import { useTranslation } from "react-i18next";
import { Button, H1, Image, Text, XStack, YStack } from "ui";
function HomeScreen() {
  // const { data } = useGqlQuery({ query, variables: {} });
  const { t } = useTranslation();
  const router = useRouter();

  return (
    <YStack
      fullscreen
      space="$15"
      w="100%"
      height="100%"
      minWidth={300}
      ai="center"
    >
      <YStack
        ai="center"
        jc="center"
        gap="$5"
        backgroundColor="$color10"
        paddingBottom="$10"
        width="100%"
        minWidth={330}
      >
        <Image
          src={WhoAmI}
          width={200}
          height={200}
          $sm={{ width: 100, height: 100, mt: "$4" }}
        />
        <H1
          fontSize="$12"
          $sm={{
            fontSize: 25,
            width: "100%",
            display: "flex",
            jc: "center",
          }}
          letterSpacing={1}
          textAlign="center"
        >
          Welcome To Who Am I
        </H1>
      </YStack>

      <YStack alignItems="center" gap="$6" jc="center" $sm={{ width: "80%" }}>
        <Text
          fontSize={40}
          $sm={{ fontSize: "$5", letterSpacing: 1, mt: "$-11" }}
          fontWeight="bold"
        >
          Let's Get Started
        </Text>

        <XStack
          gap="$5"
          paddingTop="$4"
          $sm={{
            gap: "$1",
            display: "flex",
            flexDirection: "column",
            w: "200",
            space: "$2",
          }}
        >
          <Button
            borderRadius="$2"
            borderColor="$color1"
            color="$color1"
            backgroundColor="$color12"
            fontWeight="bold"
            size="$4"
            fontSize="$4"
            hoverStyle={{
              backgroundColor: "$color10",
              // transform: "scale(1.05)",
            }}
            onPress={() => router.push("/create-room")}
            gap="$2"
            $sm={{ size: "$3" }}
          >
            Create Room
            <Image
              src={MeetingIcon}
              width={30}
              height={30}
              $sm={{ width: "$2", height: "$2" }}
            />
          </Button>
          <Button
            borderRadius="$2"
            borderColor="$color1"
            color="$color1"
            backgroundColor="$color12"
            fontWeight="bold"
            size="$4"
            fontSize="$4"
            hoverStyle={{
              backgroundColor: "$color10",
            }}
            onPress={() => router.push("/join-room")}
            gap="$2"
            $sm={{ size: "$3" }}
          >
            Join Room
            <Image
              src={PlusIcon}
              width={30}
              height={30}
              $sm={{ width: "$2", height: "$2", ml: "$5" }}
            />
          </Button>
        </XStack>
      </YStack>
      {/* <YStack>
        <CreateQuestion />
      </YStack> */}
    </YStack>
  );
}

export const Screen = withAuthenticated(HomeScreen);
// export const Screen =HomeScreen
