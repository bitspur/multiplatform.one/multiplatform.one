/**
 * File: /src/providers/providers.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 01-01-1970 00:00:00
 * Author: niharikamoluguri
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { createAnimations as createAnimationsCss } from "@tamagui/animations-css";
import { act, render } from "@testing-library/react";
import jest from "jest-mock";
import type { TamaguiInternalConfig } from "tamagui";
import {
  AnimationDriverTogglerContext,
  AnimationDriverTogglerContextProvider,
} from "./AnimationDriverTogglerContextProvider";

const mockTamaguiConfig: TamaguiInternalConfig = {
  animations: createAnimationsCss({
    fade: "ease-out 300ms",
    slide: "ease-in 200ms",
    bouncy: "ease-in 200ms",
    lazy: "ease-in 600ms",
    slow: "ease-in 500ms",
    quick: "ease-in 100ms",
    tooltip: "ease-in 400ms",
  }),
  themes: {
    light: {
      background: {
        val: "#ffffff",
        name: "backgroundLight",
        key: "color",
        isVar: true,
      },
      color: { val: "#000000", name: "textLight", key: "color", isVar: true },
    },
    dark: {
      background: {
        val: "#000000",
        name: "backgroundDark",
        key: "color",
        isVar: true,
      },
      color: { val: "#ffffff", name: "textDark", key: "color", isVar: true },
    },
  },
  tokens: {
    color: {
      primary: {
        val: "#6200ee",
        key: "color",
        isVar: true as const,
        name: "colorPrimary",
      },
      secondary: {
        val: "#03dac6",
        key: "color",
        isVar: true as const,
        name: "colorSecondary",
      },
      error: {
        val: "#b00020",
        key: "color",
        isVar: true as const,
        name: "colorError",
      },
    },
    space: {
      small: { val: 8, key: "space", isVar: true as const, name: "spaceSmall" },
      medium: {
        val: 16,
        key: "space",
        isVar: true as const,
        name: "spaceMedium",
      },
      large: {
        val: 24,
        key: "space",
        isVar: true as const,
        name: "spaceLarge",
      },
    },
    size: {
      small: { val: 12, key: "size", isVar: true as const, name: "sizeSmall" },
      medium: {
        val: 16,
        key: "size",
        isVar: true as const,
        name: "sizeMedium",
      },
      large: { val: 20, key: "size", isVar: true as const, name: "sizeLarge" },
    },
    radius: {
      small: {
        val: 4,
        key: "radius",
        isVar: true as const,
        name: "radiusSmall",
      },
      medium: {
        val: 8,
        key: "radius",
        isVar: true as const,
        name: "radiusMedium",
      },
      large: {
        val: 16,
        key: "radius",
        isVar: true as const,
        name: "radiusLarge",
      },
    },
    zIndex: {
      low: { val: 1, key: "zIndex", isVar: true as const, name: "zIndexLow" },
      medium: {
        val: 5,
        key: "zIndex",
        isVar: true as const,
        name: "zIndexMedium",
      },
      high: {
        val: 10,
        key: "zIndex",
        isVar: true as const,
        name: "zIndexHigh",
      },
    },
  },
  tokensParsed: {
    color: {},
    space: {},
    size: {},
    radius: {},
    zIndex: {},
  },
  themeConfig: {
    light: {},
    dark: {},
  },
  fontsParsed: {
    body: { size: {} },
    heading: { size: {} },
  },
  getCSS: () => "",
  getNewCSS: () => "",
  parsed: false,
  inverseShorthands: {},
  fontSizeTokens: new Set(["small", "medium", "large"]),
  specificTokens: {},
  defaultFontToken: "body",
  shorthands: {
    p: ["padding"],
    m: ["margin"],
    bg: ["backgroundColor"],
  },
  fonts: {
    body: {
      family: "Arial, sans-serif",
      size: { base: 16 },
      weight: { base: "400" },
    },
    heading: {
      family: "Helvetica, sans-serif",
      size: { base: 24 },
      weight: { base: "700" },
    },
  },
  fontLanguages: ["latin", "cyrillic", "greek"],
  media: {
    sm: { maxWidth: 640 },
    md: { minWidth: 641, maxWidth: 1024 },
    lg: { minWidth: 1025 },
  },
  settings: {},
};
const cssAnimations = createAnimationsCss({
  bouncy: "ease-in 200ms",
  lazy: "ease-in 600ms",
  slow: "ease-in 500ms",
  quick: "ease-in 100ms",
  tooltip: "ease-in 400ms",
});

describe("AnimationDriverTogglerContextProvider", () => {
  it("toggles to the next driver correctly", () => {
    let contextValues: any;
    render(
      <AnimationDriverTogglerContextProvider tamaguiConfig={mockTamaguiConfig}>
        <AnimationDriverTogglerContext.Consumer>
          {(value) => {
            contextValues = value;
            return null;
          }}
        </AnimationDriverTogglerContext.Consumer>
      </AnimationDriverTogglerContextProvider>,
    );
    expect(contextValues.driverName).toBe("react-native");
    act(() => {
      contextValues.nextDriver();
    });
    expect(contextValues.driverName).toBe("css");
    expect(Object.keys(contextValues.driver)).toEqual(
      Object.keys(cssAnimations),
    );
    act(() => {
      contextValues.nextDriver();
    });
    expect(contextValues.driverName).toBe("react-native");
  });

  it("sets driver name correctly using setDriverName", () => {
    let contextValues: any;
    render(
      <AnimationDriverTogglerContextProvider tamaguiConfig={mockTamaguiConfig}>
        <AnimationDriverTogglerContext.Consumer>
          {(value) => {
            contextValues = value;
            return null;
          }}
        </AnimationDriverTogglerContext.Consumer>
      </AnimationDriverTogglerContextProvider>,
    );
    act(() => {
      contextValues.setDriverName("css");
    });
    expect(contextValues.driverName).toBe("css");
    expect(Object.keys(contextValues.driver)).toEqual(
      Object.keys(cssAnimations),
    );
    act(() => {
      contextValues.setDriverName("react-native");
    });
    expect(contextValues.driverName).toBe("react-native");
  });
});
