/**
 * File: /src/ErrorBoundary/ErrorBoundary.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 04-02-2025 09:53:47
 *
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { fireEvent, render, screen } from "@testing-library/react";
import { H1 } from "tamagui";
import { expect, vitest } from "vitest";
import { ErrorBoundary } from "./index";

const ChildThatThrows = () => {
  throw new Error("Test Error");
};
const ChildThatRenders = () => {
  return <H1>Hello from child!</H1>;
};

describe("ErrorBoundary", () => {
  it("should render children when no error occurs", () => {
    render(
      <ErrorBoundary>
        <ChildThatRenders />
      </ErrorBoundary>,
    );
    expect(screen.getByText("Hello from child!")).toBeInTheDocument();
  });

  it("should show error message and retry button when an error occurs", () => {
    render(
      <ErrorBoundary>
        <ChildThatThrows />
      </ErrorBoundary>,
    );
    expect(screen.getByText("Oops, there is an error!")).toBeInTheDocument();
    expect(screen.getByText("Try again?")).toBeInTheDocument();
  });

  it("should reset error state and re-render children when retry button is clicked", async () => {
    render(
      <ErrorBoundary>
        <ChildThatThrows />
      </ErrorBoundary>,
    );
    expect(screen.getByText("Oops, there is an error!")).toBeInTheDocument();
    expect(screen.getByText("Try again?")).toBeInTheDocument();
    fireEvent.click(screen.getByText("Try again?"));
  });

  it("should log errors when componentDidCatch is triggered", () => {
    const spy = vitest.spyOn(console, "info");
    render(
      <ErrorBoundary>
        <ChildThatThrows />
      </ErrorBoundary>,
    );
    expect(spy).toHaveBeenCalledWith(
      expect.objectContaining({
        error: expect.any(Error),
        errorInfo: expect.any(Object),
      }),
    );
    spy.mockRestore();
  });
});
