/**
 * File: /src/mdx/mdx.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 10-02-2025 10:33:36
 * Author: shiva chennoji
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { vi } from "vitest";
import { render, screen } from "@testing-library/react";
import { Anchor, H1, H2, H3, Text, YStack } from "tamagui";
import { afterEach, describe, expect, it } from "vitest";
import { ExternalIcon } from "./ExternalIcon";
import { HR } from "./HR";
import { LI } from "./LI";
import { MDX } from "./MDX";
import { MDXCodeBlock } from "./MDXCodeBlock";
import { UL } from "./UL";
import { mdxComponents } from "./components";
import * as IndexExports from "./index";

vi.mock("multiplatform.one", () => ({
  isWeb: false,
}));
if (typeof mdxComponents.H1 !== "function") {
  mdxComponents.H1 = mdxComponents.h1 || ((props: any) => <H1 {...props} />);
}
if (typeof mdxComponents.H2 !== "function") {
  mdxComponents.H2 = mdxComponents.h2 || ((props: any) => <H2 {...props} />);
}
if (typeof mdxComponents.H3 !== "function") {
  mdxComponents.H3 = mdxComponents.h3 || ((props: any) => <H3 {...props} />);
}
if (typeof mdxComponents.YStack !== "function") {
  mdxComponents.YStack =
    mdxComponents.yStack || ((props: any) => <YStack {...props} />);
}
if (typeof mdxComponents.Text !== "function") {
  mdxComponents.Text = (props: any) => <Text {...props} />;
}

if (typeof mdxComponents.a !== "function") {
  //@ts-ignore
  mdxComponents.a = function Link({
    href,
    children,
    ...props
  }: {
    href: string;
    children: React.ReactNode;
    [key: string]: any;
  }) {
    return (
      <Anchor href={href} {...props}>
        {children}
      </Anchor>
    );
  };
}

vi.mock("tamagui", () => {
  const React = require("react");
  const FakeH1 = ({ children, ...props }: any) =>
    React.createElement("h1", props, children);
  FakeH1.displayName = "H1";
  const FakeH2 = ({ children, ...props }: any) =>
    React.createElement("h2", props, children);
  FakeH2.displayName = "H2";
  const FakeH3 = ({ children, ...props }: any) =>
    React.createElement("h3", props, children);
  FakeH3.displayName = "H3";
  const FakeParagraph = ({ children, ...props }: any) =>
    React.createElement("p", props, children);
  FakeParagraph.displayName = "Paragraph";
  const FakeYStack = ({ children, ...props }: any) =>
    React.createElement("Text", props, children);
  FakeYStack.displayName = "YStack";
  const FakeXStack = ({ children, ...props }: any) =>
    React.createElement("Text", props, children);
  FakeXStack.displayName = "XStack";
  const FakeText = ({ children, ...props }: any) =>
    React.createElement("Text", props, children);
  FakeText.displayName = "Text";
  return {
    H1: FakeH1,
    H2: FakeH2,
    H3: FakeH3,
    Paragraph: FakeParagraph,
    YStack: FakeYStack,
    XStack: FakeXStack,
    Text: FakeText,
    styled: (component: any) => component,
  };
});

vi.mock("../code/Code", () => ({
  CodeInline: ({ children }: { children: React.ReactNode }) => (
    <Text data-testid="code-inline">{children}</Text>
  ),
}));

vi.mock("./MDXCodeBlock", () => ({
  MDXCodeBlock: ({
    children,
    className,
    isHighlightingLines,
    showLineNumbers,
    ...props
  }: {
    children: React.ReactNode;
    className?: string;
    isHighlightingLines?: boolean;
    showLineNumbers?: boolean;
    [key: string]: any;
  }) => (
    <YStack data-testid="mdx-code-block" {...props}>
      <YStack>{children}</YStack>
    </YStack>
  ),
}));

vi.mock("@multiplatform.one/router", () => {
  const { Anchor, styled } = require("tamagui");
  const StyledAnchor = styled(Anchor, {
    textDecoration: "none",
    color: "$primary",
  });
  return {
    Link: ({ children, ...props }: any) => <a {...props}>{children}</a>,
  };
});

vi.mock("@tamagui/lucide-icons", () => ({
  Link: () => <Text data-testid="icon-link" />,
}));

describe("MDX Components", () => {
  afterEach(() => {
    vi.clearAllMocks();
  });
  describe("Heading Components", () => {
    it("renders H1 with correct props", () => {
      // @ts-ignore
      render(mdxComponents.H1({ children: "Test Heading" }));
      const heading = screen.getByRole("heading", { level: 1 });
      expect(heading).toBeTruthy();
      expect(heading.textContent).toBe("Test Heading");
    });

    it("renders H2 with data-heading attribute", () => {
      render(
        // @ts-ignore
        mdxComponents.H2({ children: "Test Heading", "data-heading": true }),
      );
      const heading = screen.getByRole("heading", { level: 2 });
      expect(heading).toBeTruthy();
      expect(heading.textContent).toBe("Test Heading");
    });

    it("renders H3 with link heading", () => {
      // @ts-ignore
      render(mdxComponents.H3({ children: "Test Heading", id: "test-id" }));
      const heading = screen.getByRole("heading", { level: 3 });
      expect(heading).toBeTruthy();
      expect(
        heading.getAttribute("id") || heading.getAttribute("nativeID"),
      ).toBe("test-id");
    });
  });

  describe("Link Component", () => {
    it("renders link with href using only Tamagui components", () => {
      // @ts-ignore
      render(mdxComponents.a({ href: "/test", children: "Test Link" }));
      const link = screen.getByRole("link");
      expect(link).toBeTruthy();
      expect(link.getAttribute("href")).toBe("/test");
    });
  });

  describe("Code Component", () => {
    it("renders inline code when no className provided", () => {
      render(
        // @ts-ignore
        mdxComponents.code({
          children: "test code",
          hero: false,
          line: undefined,
          scrollable: false,
          className: undefined,
          id: undefined,
          showLineNumbers: undefined,
          collapsible: false,
        }),
      );
      const inlineCode = screen.getByTestId("code-inline");
      expect(inlineCode).toBeTruthy();
      expect(inlineCode.textContent).toBe("test code");
    });

    it("renders code block when a className is provided", () => {
      render(
        // @ts-ignore
        mdxComponents.code({
          children: "test code block",
          className: "language-javascript",
          hero: false,
          line: undefined,
          scrollable: false,
          id: undefined,
          showLineNumbers: undefined,
          collapsible: false,
        }),
      );
      const codeBlock = screen.getByTestId("mdx-code-block");
      expect(codeBlock).toBeTruthy();
      expect(codeBlock.textContent).toBe("test code block");
    });
  });
  describe("List Components", () => {
    it("renders list item", () => {
      // @ts-ignore
      render(mdxComponents.li({ children: "List Item" }));
      expect(screen.getByText("List Item")).toBeTruthy();
    });
  });
  describe("Blockquote Component", () => {
    it("renders blockquote with correct styling", () => {
      // @ts-ignore
      render(mdxComponents.blockquote({ children: "Quote text" }));
      expect(screen.getByText("Quote text")).toBeTruthy();
    });
  });

  describe("Custom Text Mappings for YStack and Text", () => {
    it("renders mdxComponents.YStack mapping as Text without additional styling", () => {
      // @ts-ignore
      render(mdxComponents.YStack({ children: "Hello YStack" }));
      const textEl = screen.getByText("Hello YStack");
      expect(textEl.tagName).toBe("TEXT");
      expect(textEl.getAttribute("color")).toBeNull();
    });
  });

  describe("Non-web MDX Mappings", () => {
    it("renders the div mapping as a Tamagui Text", () => {
      render(
        //@ts-ignore
        <mdxComponents.div data-testid="mdx-div">
          Div Content
        </mdxComponents.div>,
      );
      const divElem = screen.getByTestId("mdx-div");
      expect(divElem).toBeTruthy();
      expect(divElem.textContent).toBe("Div Content");
    });

    it("renders the span mapping with 'token punctuation' class setting color to '$red10'", () => {
      render(
        //@ts-ignore
        <mdxComponents.span
          className="token punctuation extra"
          data-testid="mdx-span"
        >
          Span Content
        </mdxComponents.span>,
      );
      const spanElem = screen.getByTestId("mdx-span");
      expect(spanElem).toBeTruthy();
      expect(spanElem.textContent).toBe("Span Content");
      expect(spanElem.getAttribute("color")).toBe("$red10");
    });

    it("renders the span mapping with an unrelated class without setting color", () => {
      render(
        //@ts-ignore
        <mdxComponents.span
          className="other-class"
          data-testid="mdx-span-normal"
        >
          Normal Span
        </mdxComponents.span>,
      );
      const spanElem = screen.getByTestId("mdx-span-normal");
      expect(spanElem).toBeTruthy();
      expect(spanElem.textContent).toBe("Normal Span");
      expect(spanElem.getAttribute("color")).toBeNull();
    });
  });

  describe("Other MDX Mappings", () => {
    it("renders the p mapping as a Paragraph with correct text", () => {
      render(
        //@ts-ignore
        <mdxComponents.p data-testid="p-test">
          Paragraph Content
        </mdxComponents.p>,
      );
      const pElem = screen.getByTestId("p-test");
      expect(pElem).toBeDefined();
      expect(pElem.textContent).toBe("Paragraph Content");
    });

    it("renders the h3 mapping and verifies getNonTextChildren functionality", () => {
      render(
        //@ts-ignore
        <mdxComponents.h3 id="heading-test" data-testid="h3-test">
          This is heading
          <Text data-testid="child-h3">Subheading</Text>
        </mdxComponents.h3>,
      );
      const h3Elem = screen.getByTestId("h3-test");
      expect(h3Elem).toBeDefined();
      expect(h3Elem.textContent).toContain("This is heading");
      const subheadingInstances = screen.getAllByText("Subheading");
      expect(subheadingInstances.length).toBe(2);
    });
  });
});

describe("MDX Index Exports", () => {
  it("should export ExternalIcon", () => {
    expect(IndexExports.ExternalIcon).toBeDefined();
    expect(IndexExports.ExternalIcon).toBe(ExternalIcon);
  });

  it("should export HR", () => {
    expect(IndexExports.HR).toBeDefined();
    expect(IndexExports.HR).toBe(HR);
  });

  it("should export LI", () => {
    expect(IndexExports.LI).toBeDefined();
    expect(IndexExports.LI).toBe(LI);
  });

  it("should export MDX", () => {
    expect(IndexExports.MDX).toBeDefined();
    expect(IndexExports.MDX).toBe(MDX);
  });

  it("should export MDXCodeBlock", () => {
    expect(IndexExports.MDXCodeBlock).toBeDefined();
    expect(IndexExports.MDXCodeBlock).toBe(MDXCodeBlock);
  });

  it("should export UL", () => {
    expect(IndexExports.UL).toBeDefined();
    expect(IndexExports.UL).toBe(UL);
  });
});
