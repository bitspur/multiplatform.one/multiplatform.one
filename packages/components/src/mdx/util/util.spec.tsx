/**
 * File: /src/mdx/util/util.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 07-02-2025 04:32:57
 * Author: Lavanya Katari
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { visit } from "unist-util-visit";
import { describe, expect, it } from "vitest";
import { rehypeHighlightCode } from "./rehypeHighlightCode";
import { rehypeMetaAttributes } from "./rehypeMetaAttributes";

const mockAST = {
  type: "root",
  children: [
    {
      type: "element",
      tagName: "pre",
      children: [
        {
          type: "element",
          tagName: "code",
          properties: {
            className: ["language-tsx"],
          },
          data: {
            meta: 'title="Example Code" highlight="1-2" showLineNumbers',
          },
          children: [
            {
              type: "text",
              value: "const x: number = 42;\nconsole.log(x);",
            },
          ],
        },
      ],
    },
  ],
};

describe("rehypeMetaAttributes and rehypeHighlightCode", () => {
  it("should extract and apply meta attributes as properties", () => {
    const transformer = rehypeMetaAttributes();
    transformer(mockAST);
    let foundCodeNode = false;
    visit(mockAST, "element", (node: any) => {
      if (node.tagName === "code") {
        foundCodeNode = true;
        expect(node.properties).toHaveProperty("title", "Example Code");
        expect(node.properties).toHaveProperty("highlight", "1-2");
        expect(node.properties).toHaveProperty("showLineNumbers", "");
      }
    });
    expect(foundCodeNode).toBe(true);
  });

  it("should handle multiple attributes correctly", () => {
    const transformer = rehypeMetaAttributes();
    transformer(mockAST);
    visit(mockAST, "element", (node: any) => {
      if (node.tagName === "code") {
        expect(Object.keys(node.properties)).toEqual(
          expect.arrayContaining([
            "className",
            "title",
            "highlight",
            "showLineNumbers",
          ]),
        );
      }
    });
  });

  it("should not modify nodes without meta attributes", () => {
    const originalAST = JSON.parse(JSON.stringify(mockAST));
    originalAST.children[0].children[0].data = undefined;
    originalAST.children[0].children[0].properties = {
      className: ["language-tsx"],
    };
    const transformer = rehypeMetaAttributes();
    transformer(originalAST);
    visit(originalAST, "element", (node) => {
      if (node.tagName === "code") {
        expect(node.properties).toEqual({ className: ["language-tsx"] });
      }
    });
  });

  it("should handle empty meta attributes without breaking", () => {
    const emptyMetaAST = JSON.parse(JSON.stringify(mockAST));
    emptyMetaAST.children[0].children[0].data.meta = "";
    emptyMetaAST.children[0].children[0].properties = {
      className: ["language-tsx"],
    };
    const transformer = rehypeMetaAttributes();
    transformer(emptyMetaAST);
    visit(emptyMetaAST, "element", (node) => {
      if (node.tagName === "code") {
        expect(node.properties).toEqual({ className: ["language-tsx"] });
      }
    });
  });

  it("should correctly highlight specific lines", () => {
    const transformer = rehypeHighlightCode();
    transformer(mockAST);
    let highlightedLines = 0;
    visit(mockAST, "element", (node: any) => {
      if (
        node.properties &&
        Array.isArray(node.properties.className) &&
        node.properties.className.includes("highlight-line")
      ) {
        highlightedLines++;
      }
    });
    expect(highlightedLines).toBe(2);
  });
});
