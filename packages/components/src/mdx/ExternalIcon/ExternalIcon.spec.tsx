/**
 * File: /src/mdx/ExternalIcon/ExternalIcon.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 04-02-2025 10:03:22
 * Author: shiva chennoji 7
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { render } from "@testing-library/react";
import { describe, expect, it } from "vitest";
import { ExternalIcon } from "./index";

describe("ExternalIcon Component", () => {
  it("renders without crashing", () => {
    const { container } = render(<ExternalIcon />);
    const svgElement = container.querySelector("svg");
    expect(svgElement).toBeTruthy();
  });

  it("renders Svg with correct attributes", () => {
    const { container } = render(<ExternalIcon />);
    const svgElement = container.querySelector("svg");
    expect(svgElement).toHaveAttribute("height", "100");
    expect(svgElement).toHaveAttribute("width", "100");
    expect(svgElement).toHaveAttribute("viewBox", "0 0 15 15");
  });

  it("applies correct opacity style to Svg", () => {
    const { container } = render(<ExternalIcon />);
    const svgElement = container.querySelector("svg");
    expect(svgElement).toHaveStyle("opacity: 0.4");
  });

  it("contains Path inside Svg", () => {
    const { container } = render(<ExternalIcon />);
    const pathElement = container.querySelector("svg path");
    expect(pathElement).toBeTruthy();
    expect(pathElement).toHaveAttribute("fill", "var(--color)");
  });
});
