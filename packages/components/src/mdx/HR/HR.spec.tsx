/**
 * File: /src/mdx/HR/HR.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 04-02-2025 05:10:40
 * Author: shiva chennoji 7
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { render } from "@testing-library/react";
import { TamaguiProvider } from "tamagui";
import { describe, expect, it } from "vitest";
import { HR } from "./index";

const tamaguiConfig: any = {
  tokens: {},
  themes: { default: {} },
  getCSS: () => "",
  disableCSS: true,
};
function renderWithTamagui(ui: React.ReactElement) {
  return render(<TamaguiProvider config={tamaguiConfig}>{ui}</TamaguiProvider>);
}

it("renders without crashing", () => {
  const { container } = renderWithTamagui(<HR />);
  expect(container).toBeTruthy();
});

it("renders outer YStack with correct basic alignment and structure", () => {
  const { container } = renderWithTamagui(<HR />);
  const outerYStack = container.firstElementChild as HTMLElement;
  expect(outerYStack).toBeTruthy();
  const styleAttr = outerYStack.getAttribute("style") || "";
  expect(styleAttr.includes("margin-left: auto"));
  expect(styleAttr.includes("margin-right: auto"));
  expect(styleAttr.includes("max-width: 50%"));
});
