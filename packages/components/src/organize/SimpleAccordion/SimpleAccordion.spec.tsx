/**
 * File: /src/organize/SimpleAccordion/SimpleAccordion.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 03-02-2025 04:43:15
 * Author: ffx
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { fireEvent, render, screen } from "@testing-library/react";
import { Text, YStack } from "tamagui";
import { describe, expect, it } from "vitest";
import { AccordionItem, SimpleAccordion } from "./index";

describe("SimpleAccordion Component", () => {
  it("should render successfully", () => {
    const { container } = render(
      <SimpleAccordion>
        <AccordionItem
          value="item1"
          trigger={(open) => <span>{open ? "Hide" : "Show"}</span>}
        >
          <YStack>
            <Text>Content 1</Text>
          </YStack>
        </AccordionItem>
      </SimpleAccordion>,
    );
    expect(container).toBeDefined();
  });

  it("should render the trigger correctly", () => {
    render(
      <SimpleAccordion>
        <AccordionItem
          value="item1"
          trigger={(open) => <span>{open ? "Hide" : "Show"}</span>}
        >
          <YStack>
            <Text>Content 1</Text>
          </YStack>
        </AccordionItem>
      </SimpleAccordion>,
    );
    expect(screen.getByText("Show")).toBeInTheDocument();
  });

  it("should render the content when opened", () => {
    render(
      <SimpleAccordion>
        <AccordionItem
          value="item1"
          trigger={(open) => <span>{open ? "Hide" : "Show"}</span>}
        >
          <YStack>
            <Text>Content 1</Text>
          </YStack>
        </AccordionItem>
      </SimpleAccordion>,
    );
    expect(screen.queryByText("Content 1")).not.toBeInTheDocument();
    fireEvent.click(screen.getByText("Show"));
    expect(screen.getByText("Content 1")).toBeInTheDocument();
  });

  it("should toggle content visibility when trigger is clicked", () => {
    render(
      <SimpleAccordion>
        <AccordionItem
          value="item1"
          trigger={(open) => <span>{open ? "Hide" : "Show"}</span>}
        >
          <YStack>
            <Text>Content 1</Text>
          </YStack>
        </AccordionItem>
      </SimpleAccordion>,
    );
    fireEvent.click(screen.getByText("Show"));
    expect(screen.getByText("Content 1")).toBeInTheDocument();
    fireEvent.click(screen.getByText("Hide"));
    expect(screen.queryByText("Content 1")).not.toBeInTheDocument();
  });

  it("should allow multiple items to be opened", () => {
    render(
      <SimpleAccordion>
        <AccordionItem
          value="item1"
          trigger={(open) => <span>{open ? "Hide 1" : "Show 1"}</span>}
        >
          <YStack>
            <Text>Content 1</Text>
          </YStack>
        </AccordionItem>
        <AccordionItem
          value="item2"
          trigger={(open) => <span>{open ? "Hide 2" : "Show 2"}</span>}
        >
          <YStack>
            <Text>Content 2</Text>
          </YStack>
        </AccordionItem>
      </SimpleAccordion>,
    );
    fireEvent.click(screen.getByText("Show 1"));
    expect(screen.getByText("Content 1")).toBeInTheDocument();
    fireEvent.click(screen.getByText("Show 2"));
    expect(screen.getByText("Content 2")).toBeInTheDocument();
  });

  it("should have appropriate ARIA roles", () => {
    render(
      <SimpleAccordion>
        <AccordionItem
          value="item3"
          trigger={(open) => <span>{open ? "Hide" : "Show"}</span>}
        >
          <YStack>
            <Text>Content 1</Text>
          </YStack>
        </AccordionItem>
      </SimpleAccordion>,
    );
    const trigger = screen.getByText("Show");
    expect(trigger).toBeInTheDocument();
    fireEvent.click(trigger);
    expect(screen.getByText("Hide")).toBeInTheDocument();
  });
});
