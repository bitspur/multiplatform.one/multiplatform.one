/**
 * File: /src/layouts/Debug/Debug.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 07-02-2025 11:42:30
 * Author: abhiramkaleru
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import {
  act,
  cleanup,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import { Paragraph, TamaguiProvider, createTamagui } from "tamagui";
import { afterEach, beforeEach, describe, expect, it, vi } from "vitest";
import { DebugLayout } from "./index";

vi.mock("multiplatform.one/theme", () => ({
  useTheme: () => [{ root: "light", sub: "blue" }, vi.fn()],
}));

vi.mock("multiplatform.one", () => ({
  useLanguage: () => ["en", vi.fn()],
  config: { get: () => "1" },
  createWithLayout: vi.fn(),
}));

vi.mock("../../tints", () => ({
  useTint: () => ({
    familiesNames: ["family1", "family2"],
    setFamily: vi.fn(),
    name: "family1",
  }),
}));

vi.mock("i18next", () => ({
  default: { languages: ["en", "es", "fr"] },
}));

vi.mock("tamagui", async () => {
  const actual = await vi.importActual("tamagui");
  return {
    ...(actual as any),
    Select: {
      Item: ({ children, value, index }: any) => (
        <Paragraph data-testid={`select-item-${value}`} data-index={index}>
          {children}
        </Paragraph>
      ),
      ItemText: ({ children }: any) => (
        <Paragraph data-testid="select-item-text">{children}</Paragraph>
      ),
    },
  };
});
const tamaguiConfig = createTamagui(config);

describe("DebugLayout", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };
  beforeEach(() => {
    vi.clearAllMocks();
  });
  afterEach(cleanup);

  it("renders children correctly", () => {
    renderWithProviders(
      <DebugLayout>
        <Paragraph data-testid="child">Test Content</Paragraph>
      </DebugLayout>,
    );
    expect(screen.getByTestId("child")).toBeInTheDocument();
  });

  it("renders debug circle when DEBUG is enabled", () => {
    const { container } = renderWithProviders(
      <DebugLayout>
        <Paragraph>Content</Paragraph>
      </DebugLayout>,
    );
    const debugCircle = container.querySelector('[class*="is_Circle"]');
    expect(debugCircle).toBeInTheDocument();
  });

  it("maintains layout structure at different viewport sizes", () => {
    const { container } = renderWithProviders(
      <DebugLayout>
        <Paragraph>Responsive Content</Paragraph>
      </DebugLayout>,
    );
    const rootElement = container.querySelector('[class*="_dsp-flex"]');
    expect(rootElement).toBeInTheDocument();
  });

  it("handles root theme changes correctly", async () => {
    const { container } = renderWithProviders(
      <DebugLayout rootThemeNames={["light", "dark"]}>
        <Paragraph>Theme Test</Paragraph>
      </DebugLayout>,
    );
    const debugCircle = container.querySelector('[class*="is_Circle"]');
    expect(debugCircle).toBeInTheDocument();
    await act(async () => {
      fireEvent.click(debugCircle!);
    });
  });

  it("renders with custom size", () => {
    const customSize = 24;
    const { container } = renderWithProviders(
      <DebugLayout size={customSize}>
        <Paragraph>Custom Size Test</Paragraph>
      </DebugLayout>,
    );
    const circleContainer = container.querySelector('[class*="_w-24px"]');
    expect(circleContainer).toBeInTheDocument();
  });

  it("shows and hides popover content on click", async () => {
    const { container } = renderWithProviders(
      <DebugLayout>
        <Paragraph>Content</Paragraph>
      </DebugLayout>,
    );
    const debugCircle = container.querySelector('[class*="is_Circle"]');
    expect(debugCircle).toBeInTheDocument();
    await act(async () => {
      fireEvent.click(debugCircle!);
    });
    const popoverContent = container.querySelector('[data-state="open"]');
    expect(popoverContent).toBeInTheDocument();
  });

  it("handles sub-theme changes correctly", async () => {
    const { container } = renderWithProviders(
      <DebugLayout subThemeNames={["blue", "red"]}>
        <Paragraph>Sub-theme Test</Paragraph>
      </DebugLayout>,
    );
    const debugCircle = container.querySelector('[class*="is_Circle"]');
    expect(debugCircle).toBeInTheDocument();
    await act(async () => {
      fireEvent.click(debugCircle!);
    });
  });

  it("handles missing theme names gracefully", () => {
    const { container } = renderWithProviders(
      <DebugLayout rootThemeNames={[]}>
        <Paragraph>Error Test</Paragraph>
      </DebugLayout>,
    );
    const debugCircle = container.querySelector('[class*="is_Circle"]');
    expect(debugCircle).toBeInTheDocument();
  });

  it("handles empty theme arrays", async () => {
    const { container } = renderWithProviders(
      <DebugLayout subThemeNames={[]} rootThemeNames={[]}>
        <Paragraph>Empty Theme Test</Paragraph>
      </DebugLayout>,
    );
    const debugButton = container.querySelector('[class*="is_Circle"]');
    expect(debugButton).toBeTruthy();
    fireEvent.click(debugButton!);
    const popoverContent = await screen.findByRole("dialog");
    expect(popoverContent).toBeInTheDocument();
  });

  it("handles undefined i18n languages", async () => {
    vi.mock("i18next", () => ({
      default: { languages: undefined },
    }));
    const { container } = renderWithProviders(
      <DebugLayout>
        <Paragraph>Content</Paragraph>
      </DebugLayout>,
    );
    const debugButton = container.querySelector('[class*="is_Circle"]');
    expect(debugButton).toBeTruthy();
    fireEvent.click(debugButton!);
    const popoverContent = await screen.findByRole("dialog");
    expect(popoverContent).toBeInTheDocument();
  });

  it("should render debug button", () => {
    const { container } = render(
      <TamaguiProvider config={tamaguiConfig}>
        <DebugLayout>
          <Paragraph>Test Content</Paragraph>
        </DebugLayout>
      </TamaguiProvider>,
    );
    const debugButton = container.querySelector('[class*="is_Circle"]');
    expect(debugButton).toBeTruthy();
  });

  it("should open popover when debug button is clicked", async () => {
    const { container } = render(
      <TamaguiProvider config={tamaguiConfig}>
        <DebugLayout>
          <Paragraph>Test Content</Paragraph>
        </DebugLayout>
      </TamaguiProvider>,
    );
    const debugButton = container.querySelector('[class*="is_Circle"]');
    fireEvent.click(debugButton!);
    const dialog = await screen.findByRole("dialog");
    expect(dialog).toBeInTheDocument();
  });
});
