/**
 * File: /src/panels/SimplePopover/SimplePopover.spec.tsx
 * Project: @multiplatform.one/components
 */

import { config } from "@tamagui/config";
import { fireEvent, render } from "@testing-library/react";
import { Button, TamaguiProvider, Text, YStack, createTamagui } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { SimplePopover } from "./index";

const tamaguiConfig = createTamagui(config);

describe("SimplePopover", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };
  const defaultProps = {
    trigger: <Button>Click me</Button>,
    children: (
      <YStack>
        <Text>Popover Content</Text>
      </YStack>
    ),
    arrow: true,
  };
  const renderPopover = (props = {}) => {
    return renderWithProviders(<SimplePopover {...defaultProps} {...props} />);
  };
  describe("Basic Rendering", () => {
    it("should render successfully", () => {
      const { container } = renderPopover();
      expect(container).toBeDefined();
    });

    it("should render trigger button", () => {
      const { getByText } = renderPopover();
      expect(getByText("Click me")).toBeDefined();
    });

    it("should render with custom trigger", () => {
      const { getByText } = renderPopover({
        trigger: <Button>Custom Trigger</Button>,
      });
      expect(getByText("Custom Trigger")).toBeDefined();
    });
  });

  describe("Content Rendering", () => {
    it("should render content when triggered", async () => {
      const { getByText } = renderPopover({
        defaultOpen: true,
        children: (
          <YStack>
            <Text>Popover Content</Text>
          </YStack>
        ),
      });
      expect(getByText("Popover Content")).toBeDefined();
    });

    it("should render custom content", () => {
      const { getByText } = renderPopover({
        defaultOpen: true,
        children: (
          <YStack>
            <Text>Custom Content</Text>
          </YStack>
        ),
      });
      expect(getByText("Custom Content")).toBeDefined();
    });
  });

  describe("Arrow Handling", () => {
    it("should render with arrow by default", () => {
      const { container } = renderPopover({
        defaultOpen: true,
      });
      const arrow = container.querySelector("[data-tamagui-arrow]");
      expect(arrow).toBeDefined();
    });

    it("should render without arrow when disabled", () => {
      const { container } = renderPopover({
        defaultOpen: true,
        arrow: false,
      });
      const arrow = container.querySelector("[data-tamagui-arrow]");
      expect(arrow).toBeNull();
    });
  });

  describe("Styling", () => {
    it("should apply content styles", () => {
      const { container } = renderPopover({
        defaultOpen: true,
        contentStyle: {
          backgroundColor: "$blue10",
        },
      });
      expect(container).toBeDefined();
    });

    it("should apply trigger styles", () => {
      const { container } = renderPopover({
        triggerStyle: {
          backgroundColor: "$blue10",
        },
      });
      expect(container).toBeDefined();
    });

    it("should apply arrow styles", () => {
      const { container } = renderPopover({
        defaultOpen: true,
        arrowStyle: {
          backgroundColor: "$blue10",
        },
      });
      expect(container).toBeDefined();
    });
  });

  describe("Adaptivity", () => {
    it("should render sheet on touch platform", () => {
      const { container } = renderPopover({
        defaultOpen: true,
      });
      const sheet = container.querySelector("[data-tamagui-sheet]");
      expect(container).toBeDefined();
    });
  });

  describe("Animation", () => {
    it("should apply enter animation styles", () => {
      const { container } = renderPopover({
        defaultOpen: true,
        contentStyle: {
          enterStyle: { opacity: 0, scale: 0.9 },
        },
      });
      expect(container).toBeDefined();
    });

    it("should apply exit animation styles", () => {
      const { container } = renderPopover({
        defaultOpen: true,
        contentStyle: {
          exitStyle: { opacity: 0, scale: 0.9 },
        },
      });
      expect(container).toBeDefined();
    });
  });

  describe("Interactions", () => {
    it("should handle click events", () => {
      const onOpenChange = vi.fn();
      const { getByText } = renderPopover({
        onOpenChange,
      });
      fireEvent.click(getByText("Click me"));
      expect(onOpenChange).toHaveBeenCalled();
    });
  });

  describe("Edge Cases", () => {
    it("should handle empty content", () => {
      const { container } = renderPopover({
        defaultOpen: true,
        children: null,
      });
      expect(container).toBeDefined();
    });

    it("should handle long content", () => {
      const { container } = renderPopover({
        defaultOpen: true,
        children: (
          <YStack>
            <Text>{"Very long content ".repeat(20)}</Text>
          </YStack>
        ),
      });
      expect(container).toBeDefined();
    });
  });

  describe("Accessibility", () => {
    it("should have proper ARIA attributes", () => {
      const { container } = renderPopover({
        "aria-label": "Popover Dialog",
      });
      expect(container).toBeDefined();
    });

    it("should handle keyboard navigation", () => {
      const { container } = renderPopover();
      expect(container).toBeDefined();
    });
  });
});
