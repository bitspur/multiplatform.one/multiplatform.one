/**
 * File: /src/panels/SimpleAlertDialog/SimpleAlertDialog.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 04-02-2025 05:29:56
 * Author: Vandana Madhireddy
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.

 */

import { config } from "@tamagui/config";
import { fireEvent, render } from "@testing-library/react";
import { Button, TamaguiProvider, Text, YStack, createTamagui } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { SimpleAlertDialog } from "./index";

const tamaguiConfig = createTamagui(config);

describe("SimpleAlertDialog", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };
  const defaultProps = {
    trigger: <Button>Open Dialog</Button>,
    title: "Alert Title",
    description: "Alert Description",
    accept: "OK",
    cancel: "Cancel",
    onAccept: vi.fn(),
    onCancel: vi.fn(),
  };
  const renderDialog = (props = {}) => {
    return renderWithProviders(
      <SimpleAlertDialog {...defaultProps} {...props} />,
    );
  };

  describe("Basic Rendering", () => {
    it("should render successfully", () => {
      const { container } = renderDialog();
      expect(container).toBeDefined();
    });

    it("should render trigger button", () => {
      const { getByText } = renderDialog();
      expect(getByText("Open Dialog")).toBeDefined();
    });

    it("should render with custom trigger", () => {
      const { getByText } = renderDialog({
        trigger: <Button>Custom Trigger</Button>,
      });
      expect(getByText("Custom Trigger")).toBeDefined();
    });
  });

  describe("Content Rendering", () => {
    it("should render title and description", () => {
      const { getByText } = renderDialog({
        defaultOpen: true,
      });
      expect(getByText("Alert Title")).toBeDefined();
      expect(getByText("Alert Description")).toBeDefined();
    });

    it("should render custom content", () => {
      const { getByText } = renderDialog({
        defaultOpen: true,
        children: (
          <YStack>
            <Text>Custom Content</Text>
          </YStack>
        ),
      });
      expect(getByText("Custom Content")).toBeDefined();
    });

    it("should render custom buttons", () => {
      const { getByText } = renderDialog({
        defaultOpen: true,
        accept: "Yes",
        cancel: "No",
      });
      expect(getByText("Yes")).toBeDefined();
      expect(getByText("No")).toBeDefined();
    });
  });

  describe("Styling", () => {
    it("should apply title styles", () => {
      const { container } = renderDialog({
        defaultOpen: true,
        titleStyle: {
          color: "$blue10",
        },
      });
      expect(container).toBeDefined();
    });

    it("should apply description styles", () => {
      const { container } = renderDialog({
        defaultOpen: true,
        descriptionStyle: {
          color: "$gray10",
        },
      });
      expect(container).toBeDefined();
    });

    it("should apply content styles", () => {
      const { container } = renderDialog({
        defaultOpen: true,
        contentStyle: {
          backgroundColor: "$background",
        },
      });
      expect(container).toBeDefined();
    });

    it("should apply button styles", () => {
      const { container } = renderDialog({
        defaultOpen: true,
        buttonStyle: {
          backgroundColor: "$blue10",
        },
      });
      expect(container).toBeDefined();
    });
  });

  describe("Interactions", () => {
    it("should call onAccept when accept button clicked", () => {
      const onAccept = vi.fn();
      const { getByText } = renderDialog({
        defaultOpen: true,
        onAccept,
      });
      fireEvent.click(getByText("OK"));
      expect(onAccept).toHaveBeenCalled();
    });

    it("should call onCancel when cancel button clicked", () => {
      const onCancel = vi.fn();
      const { getByText } = renderDialog({
        defaultOpen: true,
        onCancel,
      });
      fireEvent.click(getByText("Cancel"));
      expect(onCancel).toHaveBeenCalled();
    });
  });

  describe("Animation", () => {
    it("should apply overlay animation", () => {
      const { container } = renderDialog({
        defaultOpen: true,
      });
      const overlay = container.querySelector("[data-tamagui-overlay]");
      expect(overlay).toBeDefined();
    });

    it("should apply content animation", () => {
      const { container } = renderDialog({
        defaultOpen: true,
        contentStyle: {
          enterStyle: { scale: 0.9 },
        },
      });
      expect(container).toBeDefined();
    });
  });

  describe("Edge Cases", () => {
    it("should handle missing title", () => {
      const { container } = renderDialog({
        defaultOpen: true,
        title: undefined,
      });
      expect(container).toBeDefined();
    });

    it("should handle missing description", () => {
      const { container } = renderDialog({
        defaultOpen: true,
        description: undefined,
      });
      expect(container).toBeDefined();
    });

    it("should handle long content", () => {
      const { container } = renderDialog({
        defaultOpen: true,
        description: <Text>{"Very long content ".repeat(20)}</Text>,
      });
      expect(container).toBeDefined();
    });
  });

  describe("Accessibility", () => {
    it("should have proper ARIA attributes", () => {
      const { container } = renderDialog({
        defaultOpen: true,
        "aria-label": "Alert Dialog",
      });
      expect(container).toBeDefined();
    });

    it("should handle keyboard navigation", () => {
      const { container } = renderDialog({
        defaultOpen: true,
      });
      expect(container).toBeDefined();
    });
  });
});
