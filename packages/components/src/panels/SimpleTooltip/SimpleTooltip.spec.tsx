/**
 * File: /src/panels/SimpleTooltip/SimpleTooltip.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 03-02-2025 16:29:06
 * Author: Bit Spur
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { act, cleanup, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Button, TamaguiProvider, createTamagui } from "tamagui";
import { describe, expect, it } from "vitest";
import { SimpleTooltip } from "./index";

const tamaguiConfig = createTamagui(config);

describe("SimpleTooltip", () => {
  const triggerText = "Hover me";
  const tooltipContent = "Tooltip content";
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  it("renders the trigger element", () => {
    renderWithProviders(
      <SimpleTooltip trigger={<Button>{triggerText}</Button>} arrow={false}>
        {tooltipContent}
      </SimpleTooltip>,
    );
    expect(screen.getByText(triggerText)).toBeInTheDocument();
  });

  it("displays the tooltip content on hover", async () => {
    renderWithProviders(
      <SimpleTooltip trigger={<Button>{triggerText}</Button>} arrow={false}>
        {tooltipContent}
      </SimpleTooltip>,
    );
    const triggerElement = screen.getByText(triggerText);
    await act(async () => {
      await userEvent.hover(triggerElement);
    });
    await waitFor(
      () => {
        expect(screen.getByText(tooltipContent)).toBeInTheDocument();
      },
      { timeout: 2000 },
    );
  });

  it("hides the tooltip content when not hovered", async () => {
    renderWithProviders(
      <SimpleTooltip trigger={<Button>{triggerText}</Button>} arrow={false}>
        {tooltipContent}
      </SimpleTooltip>,
    );
    const triggerElement = screen.getByText(triggerText);
    await act(async () => {
      await userEvent.hover(triggerElement);
    });
    await waitFor(() => {
      expect(screen.getByText(tooltipContent)).toBeInTheDocument();
    });
    await act(async () => {
      await userEvent.unhover(triggerElement);
    });
    await waitFor(() => {
      expect(screen.queryByText(tooltipContent)).not.toBeInTheDocument();
    });
  });

  it("does not render tooltip content if no children are provided", async () => {
    renderWithProviders(
      <SimpleTooltip trigger={<Button>{triggerText}</Button>} arrow={false}>
        {null}
      </SimpleTooltip>,
    );
    const triggerElement = screen.getByText(triggerText);
    await act(async () => {
      await userEvent.hover(triggerElement);
    });
    await waitFor(() => {
      expect(screen.queryByTestId("tooltip-content")).not.toBeInTheDocument();
    });
  });
  afterEach(cleanup);
});
