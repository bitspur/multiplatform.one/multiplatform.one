/**
 * File: /src/code/code.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 10-02-2025 06:56:56
 * Author: Lavanya Katari
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { render, screen } from "@testing-library/react";
import { TamaguiProvider, YStack } from "tamagui";
import { vi } from "vitest";
import { Code, CodeInline } from "./Code/index";

vi.mock("tamagui", async () => {
  const actual = await import("tamagui");
  return {
    ...actual,
    styled: actual.styled || ((component, styles) => component),
    TamaguiProvider: ({ children }: { children: React.ReactNode }) => (
      <YStack>{children}</YStack>
    ),
  };
});

describe("Code Component", () => {
  it("should render successfully", () => {
    render(
      <TamaguiProvider>
        <Code>const x = 10</Code>
      </TamaguiProvider>,
    );
    const codeElement = screen.getByText("const x = 10");
    expect(codeElement).toBeInTheDocument();
  });

  it("should apply default styles", () => {
    const { container } = render(
      <TamaguiProvider>
        <Code>const x = 10</Code>
      </TamaguiProvider>,
    );
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
    expect(codeElement).toHaveStyle("white-space: pre");
  });

  it("should render inline code properly", () => {
    render(
      <TamaguiProvider>
        <CodeInline>inline code</CodeInline>
      </TamaguiProvider>,
    );
    const inlineCodeElement = screen.getByText("inline code");
    expect(inlineCodeElement).toBeInTheDocument();
  });

  it("should allow custom props", () => {
    const { container } = render(
      <TamaguiProvider>
        <Code style={{ fontSize: "20px" }}>custom size</Code>
      </TamaguiProvider>,
    );
    const codeElement = container.querySelector("code");
    expect(codeElement).toBeDefined();
    expect(codeElement).toHaveStyle("font-size: 20px");
  });
});
