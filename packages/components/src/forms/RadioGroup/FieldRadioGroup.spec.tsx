/**
 * File: /src/forms/RadioGroup/FieldRadioGroup.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 30-01-2024 10:03:48
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { act } from "react-dom/test-utils";
import { TamaguiProvider, createTamagui } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { Form } from "../Form";
import { FieldRadioGroup, FieldRadioGroupItem } from "./FieldRadioGroup";
import { RadioGroup } from "./RadioGroup";

const tamaguiConfig = createTamagui(config);

describe("FieldRadioGroup", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };
  const defaultProps = {
    label: "Test Radio Group",
    name: "test",
    children: [
      <RadioGroup.Item key="1" value="1">
        Option 1
      </RadioGroup.Item>,
      <RadioGroup.Item key="2" value="2">
        Option 2
      </RadioGroup.Item>,
    ],
  };
  const renderRadioGroup = (props = {}) => {
    return renderWithProviders(
      <FieldRadioGroup {...defaultProps} {...props} />,
    );
  };
  const renderWithForm = (props = {}, formProps = {}) => {
    return renderWithProviders(
      <Form defaultValues={{ test: "" }} {...formProps}>
        <FieldRadioGroup {...defaultProps} {...props} />
      </Form>,
    );
  };

  describe("Basic Rendering", () => {
    it("should render successfully", () => {
      const { container } = renderRadioGroup();
      expect(container).toBeDefined();
    });

    it("should render with label", () => {
      const { container } = renderRadioGroup({
        label: "Custom Label",
      });
      expect(container).toBeDefined();
    });

    it("should render all radio options", () => {
      const { container } = renderRadioGroup();
      expect(container).toBeDefined();
    });
  });

  describe("Form Integration", () => {
    it("should handle form context", () => {
      const { container } = renderWithForm();
      expect(container).toBeDefined();
    });

    it("should handle default value", () => {
      const { container } = renderWithForm(
        { defaultValue: "1" },
        { defaultValues: { test: "1" } },
      );
      expect(container).toBeDefined();
    });

    it("should handle form submission", async () => {
      const onSubmit = vi.fn();
      const { container } = renderWithForm(
        {},
        {
          onSubmit: async (values) => {
            onSubmit(values);
            return undefined;
          },
        },
      );
      expect(container).toBeDefined();
      expect(onSubmit).toBeDefined();
    });
  });

  describe("Interactions", () => {
    it("should handle value changes", () => {
      const onValueChange = vi.fn();
      const { container } = renderRadioGroup({ onValueChange });
      expect(container).toBeDefined();
      expect(onValueChange).toBeDefined();
    });

    it("should handle disabled state", () => {
      const { container } = renderRadioGroup({
        disabled: true,
      });
      expect(container).toBeDefined();
    });
  });

  describe("Accessibility", () => {
    it("should handle aria attributes", () => {
      const { container } = renderRadioGroup({
        "aria-label": "Radio options",
        "aria-describedby": "radio-desc",
      });
      expect(container).toBeDefined();
    });
  });

  describe("Error Handling", () => {
    it("should display error message", () => {
      const { container } = renderRadioGroup({
        error: "This field is required",
      });
      expect(container).toBeDefined();
    });
  });

  describe("RadioGroup Event Handlers", () => {
    it("should handle blur events correctly", async () => {
      const customOnBlur = vi.fn();
      const { getByRole } = renderWithForm({
        onBlur: customOnBlur,
      });
      const radioGroup = getByRole("radiogroup");
      await act(async () => {
        await userEvent.tab();
        await userEvent.tab();
      });
      expect(customOnBlur).toHaveBeenCalled();
    });

    it("should handle value changes correctly", async () => {
      const onValueChange = vi.fn();
      const { getByText } = renderWithForm({
        onValueChange,
      });
      const option = getByText("Option 1");
      await act(async () => {
        await userEvent.click(option);
      });
      expect(onValueChange).toHaveBeenCalledWith("1");
    });

    it("should update form value when option is selected", async () => {
      const onSubmit = vi.fn();
      const { getByText } = renderWithForm(
        {},
        {
          onSubmit: async (values) => {
            onSubmit(values);
            return undefined;
          },
        },
      );
      const option = getByText("Option 1");
      await act(async () => {
        await userEvent.click(option);
      });
      expect(onSubmit).toBeDefined();
    });

    it("should handle both form and custom handlers", async () => {
      const customOnValueChange = vi.fn();
      const formOnChange = vi.fn();
      const { getByText } = renderWithForm(
        {
          onValueChange: customOnValueChange,
        },
        {
          onChange: formOnChange,
        },
      );
      const option = getByText("Option 1");
      await act(async () => {
        await userEvent.click(option);
      });
      expect(customOnValueChange).toHaveBeenCalledWith("1");
    });

    it("should maintain selected value after blur", async () => {
      const { getByText, getByRole } = renderWithForm();
      const option = getByText("Option 1");
      const radioGroup = getByRole("radiogroup");
      await act(async () => {
        await userEvent.click(option);
      });
      await act(async () => {
        await userEvent.tab();
        await userEvent.tab();
      });
      expect(option).toHaveAttribute("aria-checked", "true");
    });

    it("should handle multiple value changes", async () => {
      const onValueChange = vi.fn();
      const { getByText } = renderWithForm({
        onValueChange,
      });
      const option1 = getByText("Option 1");
      await act(async () => {
        await userEvent.click(option1);
      });
      expect(onValueChange).toHaveBeenCalledWith("1");
      const option2 = getByText("Option 2");
      await act(async () => {
        await userEvent.click(option2);
      });
      expect(onValueChange).toHaveBeenCalledWith("2");
    });

    it("should handle default values correctly", () => {
      const { getByText } = renderWithForm(
        { defaultValue: "1" },
        { defaultValues: { test: "1" } },
      );
      const option = getByText("Option 1");
      expect(option).toHaveAttribute("aria-checked", "true");
    });
  });

  describe("RadioGroupItem", () => {
    it("should render item with children", () => {
      const { getByText } = renderWithForm({
        children: [
          <FieldRadioGroupItem key="1" value="1">
            Custom Option
          </FieldRadioGroupItem>,
        ],
      });
      expect(getByText("Custom Option")).toBeDefined();
    });

    it("should pass through custom itemProps", () => {
      const { getByRole } = renderWithForm({
        children: [
          <FieldRadioGroupItem
            key="1"
            value="1"
            itemProps={{ "aria-label": "Custom Label" }}
          >
            Test Option
          </FieldRadioGroupItem>,
        ],
      });
      const radioItem = getByRole("radio");
      expect(radioItem).toHaveAttribute("aria-label", "Custom Label");
    });
  });

  describe("Standalone Usage (without Form)", () => {
    it("should render without form context", () => {
      const { getByRole } = renderWithProviders(
        <FieldRadioGroup
          label="Standalone Radio"
          value="1"
          onValueChange={vi.fn()}
        >
          <RadioGroup.Item value="1">Option 1</RadioGroup.Item>
          <RadioGroup.Item value="2">Option 2</RadioGroup.Item>
        </FieldRadioGroup>,
      );
      expect(getByRole("radiogroup")).toBeDefined();
    });

    it("should render without name prop", () => {
      const { getByRole } = renderWithProviders(
        <Form>
          <FieldRadioGroup
            label="No Name Radio"
            value="1"
            onValueChange={vi.fn()}
          >
            <RadioGroup.Item value="1">Option 1</RadioGroup.Item>
            <RadioGroup.Item value="2">Option 2</RadioGroup.Item>
          </FieldRadioGroup>
        </Form>,
      );
      expect(getByRole("radiogroup")).toBeDefined();
    });

    it("should handle value changes in standalone mode", async () => {
      const onValueChange = vi.fn();
      const { getByText } = renderWithProviders(
        <FieldRadioGroup
          label="Standalone Radio"
          value="1"
          onValueChange={onValueChange}
        >
          <RadioGroup.Item value="1">Option 1</RadioGroup.Item>
          <RadioGroup.Item value="2">Option 2</RadioGroup.Item>
        </FieldRadioGroup>,
      );
      const option2 = getByText("Option 2");
      await act(async () => {
        await userEvent.click(option2);
      });
      expect(onValueChange).toHaveBeenCalledWith("2");
    });

    it("should handle blur events in standalone mode", async () => {
      const onBlur = vi.fn();
      const { getByRole } = renderWithProviders(
        <FieldRadioGroup label="Standalone Radio" value="1" onBlur={onBlur}>
          <RadioGroup.Item value="1">Option 1</RadioGroup.Item>
          <RadioGroup.Item value="2">Option 2</RadioGroup.Item>
        </FieldRadioGroup>,
      );
      const radioGroup = getByRole("radiogroup");
      await act(async () => {
        await userEvent.tab();
        await userEvent.tab();
      });
      expect(onBlur).toHaveBeenCalled();
    });

    it("should use provided id in standalone mode", () => {
      const customId = "custom-radio-id";
      const { getByRole } = renderWithProviders(
        <FieldRadioGroup id={customId} label="Standalone Radio" value="1">
          <RadioGroup.Item value="1">Option 1</RadioGroup.Item>
          <RadioGroup.Item value="2">Option 2</RadioGroup.Item>
        </FieldRadioGroup>,
      );
      const radioGroup = getByRole("radiogroup");
      expect(radioGroup).toHaveAttribute("id", customId);
    });

    it("should handle defaultValue in standalone mode", () => {
      const { getByText } = renderWithProviders(
        <FieldRadioGroup label="Standalone Radio" defaultValue="2">
          <RadioGroup.Item value="1">Option 1</RadioGroup.Item>
          <RadioGroup.Item value="2">Option 2</RadioGroup.Item>
        </FieldRadioGroup>,
      );
      const option2 = getByText("Option 2").closest('[role="radio"]');
      expect(option2).toHaveAttribute("aria-checked", "true");
    });
  });
});
