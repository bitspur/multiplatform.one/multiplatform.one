/**
 * File: /src/forms/Progress/FieldProgress.stories.tsx
 * Project: @multiplatform.one/components
 * File Created: 19-06-2024 09:37:30
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2024
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { action } from "@storybook/addon-actions";
import { useForm } from "@tanstack/react-form";
import { Button, YStack } from "tamagui";
import { Form } from "../Form";
import type { FieldProgressProps } from "./FieldProgress";
import { FieldProgress } from "./FieldProgress";

export default {
  title: "forms/FieldProgress",
  component: FieldProgress,
  parameters: {
    status: {
      type: "beta",
    },
  },
};

export const Main = (args: FieldProgressProps) => <FieldProgress {...args} />;

Main.args = {
  label: "Upload Progress",
  name: "uploadProgress",
  value: 50,
  helperText: "File upload progress",
};

export const WithError = (args: FieldProgressProps) => (
  <FieldProgress {...args} error="Upload failed" value={30} />
);

WithError.args = {
  label: "Failed Upload",
  name: "failedUpload",
  helperText: "Error state example",
};

export const WithForm = () => {
  const form = useForm({
    defaultValues: {
      uploadProgress: 0,
    },
    onSubmit: async ({ value }) => {
      action("onSubmit")(value);
    },
  });

  return (
    <Form form={form}>
      <YStack space="$4">
        <FieldProgress
          label="Form Progress"
          name="uploadProgress"
          value={50}
          helperText="Progress in form context"
        />
        <Button onPress={form.handleSubmit}>Submit</Button>
      </YStack>
    </Form>
  );
};

export const CustomStyling = (args: FieldProgressProps) => (
  <FieldProgress
    {...args}
    indicatorProps={{
      backgroundColor: "$blue8",
    }}
    progressProps={{
      width: 300,
    }}
  />
);

CustomStyling.args = {
  label: "Custom Progress",
  name: "customProgress",
  value: 75,
  helperText: "Custom styled progress",
};
