/**
 * File: /src/forms/Progress/FieldProgress.spec.tsx
 * Project: @multiplatform.one/components
 * File Created: 30-01-2025 11:00:37
 * Author: abhiramkaleru
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { config } from "@tamagui/config";
import { useForm } from "@tanstack/react-form";
import { act, fireEvent, render, waitFor } from "@testing-library/react";
import React from "react";
import { Button, TamaguiProvider, YStack, createTamagui } from "tamagui";
import { describe, expect, it, vi } from "vitest";
import { SubmitButton } from "../Button/SubmitButton";
import { Form } from "../Form";
import { FieldProgress } from "./FieldProgress";

const tamaguiConfig = createTamagui(config);

describe("FieldProgress", () => {
  const renderWithProviders = (ui: React.ReactElement) => {
    return render(
      <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
        {ui}
      </TamaguiProvider>,
    );
  };

  describe("without form context", () => {
    it("should render with label and helper text correctly", () => {
      const { getByText, getByRole } = renderWithProviders(
        <FieldProgress
          label="Upload Progress"
          helperText="File upload in progress"
          name="uploadProgress"
          value={50}
        />,
      );
      const progressBar = getByRole("progressbar");
      const label = getByText("Upload Progress");
      const helperText = getByText("File upload in progress");
      expect(label).toBeDefined();
      expect(helperText).toBeDefined();
      expect(progressBar).toBeDefined();
      expect(progressBar).toHaveAttribute("aria-valuenow", "50");
    });

    it("should handle error state correctly", () => {
      const { getByRole } = renderWithProviders(
        <FieldProgress
          label="Upload Progress"
          name="uploadProgress"
          value={30}
          error="Upload failed"
        />,
      );
      const progressBar = getByRole("progressbar");
      expect(progressBar.parentElement).toHaveStyle({
        borderColor: "var(--red8)",
      });
    });

    it("should update progress value when changed", async () => {
      const { getByRole, rerender } = renderWithProviders(
        <FieldProgress
          label="Download Progress"
          name="downloadProgress"
          value={0}
        />,
      );
      const progressBar = getByRole("progressbar");
      expect(progressBar).toHaveAttribute("aria-valuenow", "0");
      rerender(
        <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
          <FieldProgress
            label="Download Progress"
            name="downloadProgress"
            value={75}
          />
        </TamaguiProvider>,
      );
      expect(progressBar).toHaveAttribute("aria-valuenow", "75");
    });
  });

  describe("with form context", () => {
    const FormExample = ({
      defaultValue = 0,
      name = "progress",
      onSubmit = async (_value: any) => {},
    }) => {
      return (
        <Form
          defaultValues={{
            [name]: defaultValue,
          }}
          onSubmit={async ({ value }) => {
            await onSubmit(value);
          }}
        >
          <FieldProgress label="Progress" name={name} />
          <SubmitButton>Submit</SubmitButton>
        </Form>
      );
    };

    it("should handle form submission with progress value", async () => {
      const onSubmit = vi.fn();
      const { getByText } = renderWithProviders(
        <YStack>
          <FormExample defaultValue={42} onSubmit={onSubmit} />
        </YStack>,
      );
      const submitButton = getByText("Submit");
      await act(async () => {
        fireEvent.click(submitButton);
      });
      expect(onSubmit).toHaveBeenCalledWith({ progress: 42 });
    });

    it("should handle dynamic progress updates in form context", async () => {
      let progressValue = 0;
      const TestComponent = () => {
        const [progress, setProgress] = React.useState(0);
        const simulateUpload = async () => {
          for (let i = 0; i <= 100; i += 20) {
            setProgress(i);
            progressValue = i;
            await new Promise((resolve) => setTimeout(resolve, 50));
          }
        };
        return (
          <Form
            defaultValues={{ uploadProgress: progress }}
            onSubmit={async () => {}}
          >
            <FieldProgress
              label="Upload Progress"
              name="uploadProgress"
              value={progress}
            />
            <Button onPress={simulateUpload}>Start Upload</Button>
          </Form>
        );
      };
      const { getByRole, getByText } = renderWithProviders(<TestComponent />);
      const progressBar = getByRole("progressbar");
      const startButton = getByText("Start Upload");
      expect(progressBar).toHaveAttribute("aria-valuenow", "0");
      fireEvent.click(startButton);
      await waitFor(
        () => {
          expect(progressValue).toBe(100);
          expect(progressBar).toHaveAttribute("aria-valuenow", "100");
        },
        {
          timeout: 5000,
          interval: 100,
        },
      );
    });

    it("should handle dynamic progress updates in form context (alternative)", async () => {
      const TestComponent = () => {
        const [progress, setProgress] = React.useState(0);
        React.useEffect(() => {
          let timeoutId: NodeJS.Timeout;
          if (progress < 100) {
            timeoutId = setTimeout(() => {
              setProgress((prev) => Math.min(prev + 20, 100));
            }, 100);
          }
          return () => clearTimeout(timeoutId);
        }, [progress]);
        return (
          <Form
            defaultValues={{ uploadProgress: progress }}
            onSubmit={async () => {}}
          >
            <FieldProgress
              label="Upload Progress"
              name="uploadProgress"
              value={progress}
            />
          </Form>
        );
      };
      const { getByRole } = renderWithProviders(<TestComponent />);
      const progressBar = getByRole("progressbar");
      expect(progressBar).toHaveAttribute("aria-valuenow", "0");
      await waitFor(
        () => {
          expect(progressBar).toHaveAttribute("aria-valuenow", "100");
        },
        {
          timeout: 5000,
          interval: 100,
        },
      );
    });
  });

  describe("with custom progress indicator props", () => {
    it("should apply custom indicator props correctly", () => {
      const { container } = renderWithProviders(
        <FieldProgress
          label="Custom Progress"
          name="customProgress"
          value={60}
          indicatorProps={{
            backgroundColor: "$blue8",
          }}
        />,
      );
      const indicator = container.querySelector(
        '[role="progressbar"] .css-view-175oi2r',
      );
      expect(indicator).toBeDefined();
      expect(indicator).toHaveAttribute(
        "style",
        expect.stringContaining("background-color: rgb(94, 176, 239)"),
      );
    });
  });

  describe("real-world scenarios", () => {
    it("should handle file upload simulation", async () => {
      let currentProgress = 0;
      const { getByRole, rerender } = renderWithProviders(
        <FieldProgress
          label="File Upload"
          name="fileUpload"
          value={currentProgress}
        />,
      );
      const progressBar = getByRole("progressbar");
      expect(progressBar).toHaveAttribute("aria-valuenow", "0");
      for (let progress = 0; progress <= 100; progress += 25) {
        await act(async () => {
          currentProgress = progress;
          rerender(
            <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
              <FieldProgress
                label="File Upload"
                name="fileUpload"
                value={currentProgress}
              />
            </TamaguiProvider>,
          );
        });
        expect(progressBar).toHaveAttribute(
          "aria-valuenow",
          currentProgress.toString(),
        );
      }
    });

    it("should handle multi-step form progress", async () => {
      const steps = ["Personal Info", "Address", "Review", "Submit"];
      const totalSteps = steps.length;
      let currentStep = 0;
      const { getByRole, rerender } = renderWithProviders(
        <FieldProgress
          label="Form Progress"
          name="formProgress"
          value={currentStep}
          helperText={steps[currentStep]}
        />,
      );
      const progressBar = getByRole("progressbar");
      for (let step = 0; step < totalSteps; step++) {
        await act(async () => {
          currentStep = step;
          const progress = Math.round((step / (totalSteps - 1)) * 100);
          rerender(
            <TamaguiProvider config={tamaguiConfig} defaultTheme="light">
              <FieldProgress
                label="Form Progress"
                name="formProgress"
                value={progress}
                helperText={steps[currentStep]}
              />
            </TamaguiProvider>,
          );
        });
        const progress = Math.round((currentStep / (totalSteps - 1)) * 100);
        expect(progressBar).toHaveAttribute(
          "aria-valuenow",
          progress.toString(),
        );
      }
    });
  });

  describe("FieldProgress without form context", () => {
    it("should render with default value when no form context is provided", () => {
      const { getByRole } = renderWithProviders(
        <FieldProgress name="progress" defaultValue={50} value={75} />,
      );
      const progressBar = getByRole("progressbar");
      expect(progressBar).toHaveAttribute("aria-valuenow", "75");
    });

    it("should apply error styles correctly without form context", () => {
      const { getByRole } = renderWithProviders(
        <FieldProgress name="progress" value={30} error="Error message" />,
      );
      const progressBar = getByRole("progressbar");
      expect(progressBar.parentElement).toHaveStyle({
        borderColor: "var(--red8)",
      });
    });

    it("should handle undefined form and name", () => {
      const { getByRole } = renderWithProviders(
        <FieldProgress value={50} defaultValue={25} />,
      );
      const progressBar = getByRole("progressbar");
      expect(progressBar).toHaveAttribute("aria-valuenow", "50");
    });

    it("should use defaultValue when value is not provided", () => {
      const { getByRole } = renderWithProviders(
        <FieldProgress defaultValue={30} />,
      );
      const progressBar = getByRole("progressbar");
      expect(progressBar).toHaveAttribute("aria-valuenow", "30");
    });

    it("should apply error styles from fieldProps", () => {
      const { getByRole } = renderWithProviders(
        <FieldProgress error="Error message" value={40} />,
      );
      const progressBar = getByRole("progressbar");
      expect(progressBar.parentElement).toHaveStyle({
        borderColor: "var(--red8)",
      });
    });
  });
});
