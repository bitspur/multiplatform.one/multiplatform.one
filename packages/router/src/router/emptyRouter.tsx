/**
 * File: /src/router/emptyRouter.tsx
 * Project: @multiplatform.one/router
 * File Created: 24-01-2025 16:56:49
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { Href, LinkComponent, Router } from "../types";

export const useRouter = (): Router => {
  return {
    back: () => {},
    canGoBack: () => false,
    push: () => {},
    navigate: () => {},
    replace: () => {},
    dismiss: () => {},
    dismissAll: () => {},
    canDismiss: () => false,
    setParams: () => {},
    subscribe: (listener) => {
      listener("success");
      return () => {};
    },
    onLoadState: (listener) => {
      listener("loaded");
      return () => {};
    },
  };
};

export const usePathname = (): string => "/";

export const useParams = () => ({});

export const Link: LinkComponent = ({ href, replace, children, ...props }) => (
  <a href={href as string} {...props}>
    {children}
  </a>
);

Link.resolveHref = (href: Href) => href.toString();
