/**
 * File: /src/router/index.tsx
 * Project: @multiplatform.one/router
 * File Created: 23-01-2025 15:17:57
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  Link as OneLink,
  usePathname as useOnePathname,
  useRouter as useOneRouter,
} from "one";
import type {
  Href,
  LinkComponent,
  LoadingState,
  ResultState,
  Router,
} from "../types";

const mapResultState = (state: unknown): ResultState => {
  return state === "success" ? "success" : "error";
};

const mapLoadingState = (state: unknown): LoadingState => {
  if (state === "loaded") return "loaded";
  if (state === "loading") return "loading";
  return "error";
};

export const useRouter = (): Router => {
  const router = useOneRouter();
  return {
    back: () => router.back(),
    canGoBack: () => router.canGoBack(),
    push: (href: Href) => router.push(href),
    navigate: (href: Href) => router.navigate(href),
    replace: (href: Href) => router.replace(href),
    dismiss: (count?: number) => router.dismiss(count),
    dismissAll: () => router.dismissAll(),
    canDismiss: () => router.canDismiss(),
    setParams: router.setParams,
    subscribe: (listener) => {
      const oneListener = (state: unknown) => {
        listener(mapResultState(state));
      };
      return router.subscribe(oneListener);
    },
    onLoadState: (listener) => {
      const oneListener = (state: unknown) => {
        listener(mapLoadingState(state));
      };
      return router.onLoadState(oneListener);
    },
  };
};

export const usePathname = useOnePathname;

export const useParams = () => {
  const pathname = useOnePathname();
  const searchParams = new URLSearchParams(pathname);
  const params: Record<string, string> = {};
  searchParams.forEach((value, key) => {
    params[key] = value;
  });
  return params;
};

export const Link: LinkComponent = ({
  href,
  replace,
  children,
  style,
  className,
  ...props
}) => (
  <OneLink href={href} replace={replace} {...props}>
    {children}
  </OneLink>
);

Link.resolveHref = (href: Href) => href.toString();
