/*
 * File: /src/dev.ts
 * Project: @multiplatform.one/utils
 * File Created: 17-01-2025 23:30:37
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { spawnSync } from "node:child_process";
import fs from "node:fs";
import path from "node:path";

const logger = console;

export interface LookupTranspileModulesOptions {
  log?: boolean;
}

export interface LookupTamaguiModulesOptions {
  log?: boolean;
}

export function lookupTranspileModules(
  packageDirs?: string[],
  { log = true }: LookupTranspileModulesOptions = {},
) {
  const projectRoot = lookupProjectRoot();
  const transpileModules = [
    ...new Set(
      [
        ...new Set([
          projectRoot,
          path.resolve(projectRoot, "app"),
          path.resolve(projectRoot, "packages", "ui"),
          ...(packageDirs || []),
        ]),
      ].map((packageDir) => {
        try {
          return (
            JSON.parse(
              fs.readFileSync(path.join(packageDir, "package.json"), "utf-8"),
            ).transpileModules || []
          );
        } catch (err) {
          if (log) logger.error(err);
          return [];
        }
      }),
    ),
  ].flat();
  if (log) logger.debug("transpileModules:", transpileModules.join(", "));
  return transpileModules;
}

export function lookupTamaguiModules(
  packageDirs?: string[],
  { log = true }: LookupTamaguiModulesOptions = {},
) {
  const projectRoot = lookupProjectRoot();
  const tamaguiModules = [
    ...new Set([
      "tamagui",
      ...[
        ...new Set([
          projectRoot,
          path.resolve(projectRoot, "app"),
          path.resolve(projectRoot, "packages", "ui"),
          ...(packageDirs || []),
        ]),
      ].map((packageDir) => {
        try {
          return (
            JSON.parse(
              fs.readFileSync(path.join(packageDir, "package.json"), "utf-8"),
            ).tamaguiModules || []
          );
        } catch (err) {
          if (log) logger.error(err);
          return [];
        }
      }),
    ]),
  ].flat();
  if (log) logger.debug("tamaguiModules:", tamaguiModules.join(", "));
  return tamaguiModules;
}

export function resolveConfig(
  keys: string[] = [],
): Record<string, string | undefined> {
  return keys.reduce(
    (acc: Record<string, string | undefined>, key: string) => {
      if (process.env[key]) acc[key] = process.env[key];
      return acc;
    },
    {} as Record<string, string | undefined>,
  );
}

let _projectRoot: string | undefined;
export function lookupProjectRoot() {
  if (_projectRoot) return _projectRoot;
  try {
    const { stdout } = spawnSync("git", ["rev-parse", "--show-toplevel"], {
      encoding: "utf-8",
    });
    _projectRoot = stdout.trim();
    return _projectRoot;
  } catch (err) {
    _projectRoot = process.cwd();
    return _projectRoot;
  }
}

export * from "./wait";
