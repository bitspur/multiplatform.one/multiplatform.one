/*
 * File: /src/bin/multiplatformOne.ts
 * Project: @multiplatform.one/cli
 * File Created: 10-01-2025 21:02:36
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import fsSync from "node:fs";
import fs from "node:fs/promises";
import os from "node:os";
import path from "node:path";
import { fileURLToPath } from "node:url";
import {
  formatServiceList,
  lookupProjectRoot,
  waitForApi,
  waitForFrappe,
  waitForKeycloak,
  waitForPostgres,
  waitServices,
} from "@multiplatform.one/utils/dev";
import { program } from "commander";
import dotenv from "dotenv";
import { execa } from "execa";
import inquirer from "inquirer";
import ora from "ora";
import type { CookieCutterConfig } from "../types";

const projectRoot = lookupProjectRoot();
const availableServices = ["api", "frappe", "solana", "ethereum", "sui"];
const defaultDotenvPath = path.resolve(projectRoot, ".env");
const availablePlatforms = [
  "electron",
  "expo",
  "keycloak",
  "one",
  "storybook",
  "storybook-expo",
  "vocs",
  "vscode",
  "webext",
];

process.env.COOKIECUTTER = `sh ${path.resolve(
  path.dirname(fileURLToPath(import.meta.url)),
  "../../scripts/cookiecutter.sh",
)}`;
program.name("multiplatform.one");
program.version(
  JSON.parse(
    fsSync.readFileSync(
      path.resolve(
        path.dirname(fileURLToPath(import.meta.url)),
        "../../package.json",
      ),
      "utf8",
    ),
  )?.version,
);

program
  .command("init")
  .option(
    "-r, --remote <remote>",
    "the remote to use",
    "https://gitlab.com/bitspur/multiplatform.one/cookiecutter",
  )
  .option(
    "-c, --checkout <branch>",
    "branch, tag or commit to checkout",
    "main",
  )
  .option("-p, --platforms <platforms>", "platforms to use")
  .option("-s, --services <services>", "services to use")
  .argument("[name]", "the name of the project", "")
  .description("init multiplatform.one")
  .action(async (name, options) => {
    let { services, platforms } = options;
    if (
      (
        await execa("git", ["rev-parse", "--is-inside-work-tree"], {
          reject: false,
        })
      ).exitCode === 0
    ) {
      throw new Error(
        "multiplatform.one cannot be initialized inside a git repository",
      );
    }
    if (!name) {
      name = (
        await inquirer.prompt([
          {
            message: "What is the project name?",
            name: "name",
            type: "input",
          },
        ])
      ).name;
    }
    if (!services) {
      const servicesResult = (
        await inquirer.prompt([
          {
            message: "What services are you using?",
            name: "services",
            type: "checkbox",
            choices: availableServices.map((name) => ({ name })),
          },
        ])
      ).services;
      services = servicesResult.join(",");
    }
    if (!platforms) {
      const platformsResult = (
        await inquirer.prompt([
          {
            message: "What platforms are you using?",
            name: "platforms",
            type: "checkbox",
            choices: availablePlatforms.map((name) => ({ name })),
          },
        ])
      ).platforms;
      platforms = platformsResult.join(",");
    }
    const cookieCutterConfig: CookieCutterConfig = {
      default_context: { name, platforms, services },
    };
    const cookieCutterConfigFile = path.join(
      await fs.mkdtemp(path.join(os.tmpdir(), "multiplatform-")),
      "config.json",
    );
    try {
      await fs.writeFile(
        cookieCutterConfigFile,
        JSON.stringify(cookieCutterConfig, null, 2),
      );
      await execa(
        "sh",
        [
          path.resolve(
            path.dirname(fileURLToPath(import.meta.url)),
            "../../scripts/init.sh",
          ),
          "--no-input",
          "-f",
          "--config-file",
          cookieCutterConfigFile,
          "--checkout",
          options.checkout,
          options.remote,
        ],
        {
          stdio: "inherit",
        },
      );
    } finally {
      await fs.rm(cookieCutterConfigFile, { recursive: true, force: true });
    }
  });

program
  .command("update")
  .option(
    "-r, --remote <remote>",
    "the remote to use",
    "https://gitlab.com/bitspur/multiplatform.one/cookiecutter",
  )
  .option(
    "-c, --checkout <branch>",
    "branch, tag or commit to checkout",
    "main",
  )
  .option("-p, --platforms <platforms>", "platforms to keep")
  .option("-s, --services <services>", "services to keep")
  .option("--no-prompt", "do not prompt for services and platforms")
  .description("update multiplatform.one")
  .action(
    async (options: {
      checkout: string;
      noPrompt?: boolean;
      platforms?: string;
      remote: string;
      services?: string;
    }) => {
      let platforms: string[] = [];
      let services: string[] = [];
      if (!options.platforms) {
        platforms = (
          await fs.readdir(path.join(projectRoot, "platforms"), {
            withFileTypes: true,
          })
        )
          .filter((d) => d.isDirectory())
          .map((d) => d.name)
          .filter((name) => availablePlatforms.includes(name));
      }
      if (!options.services) {
        services = (await fs.readdir(projectRoot, { withFileTypes: true }))
          .filter((d) => d.isDirectory())
          .map((d) => d.name)
          .filter((name) => availableServices.includes(name));
      }
      if (!options.noPrompt) {
        const servicesResult = await inquirer.prompt([
          {
            message: "What services are you using?",
            name: "services",
            type: "checkbox",
            choices: availableServices.map((name) => ({
              name,
              checked: services.includes(name),
            })),
          },
        ]);
        services = servicesResult.services;
        const platformsResult = await inquirer.prompt([
          {
            message: "What platforms are you using?",
            name: "platforms",
            type: "checkbox",
            choices: availablePlatforms.map((name) => ({
              name,
              checked: platforms.includes(name),
            })),
          },
        ]);
        platforms = platformsResult.platforms;
      }
      if (
        (
          await execa("git", ["rev-parse", "--is-inside-work-tree"], {
            reject: false,
          })
        ).exitCode !== 0
      ) {
        throw new Error(
          "multiplatform.one cannot be updated outside of a git repository",
        );
      }
      if (
        (
          await execa("git", ["diff", "--cached", "--quiet"], {
            reject: false,
          })
        ).exitCode !== 0
      ) {
        throw new Error(
          "multiplatform.one cannot be updated with uncommitted changes",
        );
      }
      let cookieCutterConfig: CookieCutterConfig | undefined;
      if (
        (await fs.stat(path.resolve(projectRoot, "package.json"))).isFile() &&
        (
          await fs.stat(path.resolve(projectRoot, "app/package.json"))
        ).isFile() &&
        JSON.parse(
          await fs.readFile(
            path.resolve(projectRoot, "app/package.json"),
            "utf8",
          ),
        )?.dependencies?.["multiplatform.one"]?.length
      ) {
        const name = JSON.parse(
          await fs.readFile(path.resolve(projectRoot, "package.json"), "utf8"),
        )?.name;
        if (name) {
          cookieCutterConfig = {
            default_context: {
              name,
              platforms: platforms.join(","),
              services: services.join(","),
            },
          };
        }
      }
      if (!cookieCutterConfig) {
        throw new Error("not a multiplatform.one project");
      }
      const cookieCutterConfigFile = path.join(
        await fs.mkdtemp(path.join(os.tmpdir(), "multiplatform-")),
        "config.json",
      );
      try {
        await fs.writeFile(
          cookieCutterConfigFile,
          JSON.stringify(cookieCutterConfig, null, 2),
        );
        await execa(
          "sh",
          [
            path.resolve(
              path.dirname(fileURLToPath(import.meta.url)),
              "../../scripts/update.sh",
            ),
            "--no-input",
            "-f",
            "--config-file",
            cookieCutterConfigFile,
            "--checkout",
            options.checkout,
            options.remote,
          ],
          {
            cwd: projectRoot,
            stdio: "inherit",
            shell: true,
          },
        );
      } finally {
        await fs.rm(cookieCutterConfigFile, { recursive: true, force: true });
      }
    },
  );

program
  .command("wait")
  .description("wait for a service to be ready")
  .option("-i, --interval <interval>", "interval to wait for", "1000")
  .option("-t, --timeout <timeout>", "timeout to wait for", "600000")
  .option("-e, --dotenv <dotenv>", "dotenv file path", ".env")
  .argument(
    "<services>",
    `the services to wait for (${waitServices.join(", ")})`,
  )
  .action(async (servicesString, options) => {
    dotenv.config({ path: options.dotenv || defaultDotenvPath });
    const interval = Number.parseInt(options.interval);
    const timeout = Number.parseInt(options.timeout);
    const services: string[] = servicesString.split(",");
    try {
      await waitWithSpinner(services, { interval, timeout });
    } catch (err) {
      process.exit(1);
    }
  });

program
  .command("mesh")
  .description("start the mesh server")
  .option("-a, --api", "use api", false)
  .option("-e, --dotenv <dotenv>", "dotenv file path", ".env")
  .option("-f, --frappe", "use frappe", false)
  .option("-i, --interval <interval>", "interval to wait for", "1000")
  .option("-p, --port <port>", "port to run mesh on", "5002")
  .option("-t, --timeout <timeout>", "timeout to wait for", "600000")
  .action(async (options) => {
    dotenv.config({ path: options.dotenv || defaultDotenvPath });
    process.env.UWS_HTTP_MAX_HEADERS_SIZE = "16384";
    if (options.frappe || options.api) {
      process.env.MESH_API = options.api ? "1" : "0";
      process.env.MESH_FRAPPE = options.frappe ? "1" : "0";
    }
    if (
      !(await fs
        .stat(path.resolve(projectRoot, "app/main.ts"))
        .catch(() => false))
    ) {
      process.env.MESH_APP = "0";
    }
    if (
      !(await fs
        .stat(path.resolve(projectRoot, "frappe/package.json"))
        .catch(() => false))
    ) {
      process.env.MESH_FRAPPE = "0";
    }
    const services = [
      ...(process.env.MESH_API === "1" ? ["api"] : []),
      ...(process.env.MESH_FRAPPE === "1" ? ["frappe"] : []),
    ];
    if (services.length > 0) {
      const interval = Number.parseInt(options.interval);
      const timeout = Number.parseInt(options.timeout);
      try {
        await waitWithSpinner(services, { interval, timeout });
      } catch (err) {
        process.exit(1);
      }
    }
    await execa(
      "mesh",
      [
        "dev",
        "--port",
        Number(options.port || process.env.MESH_PORT || 5002).toString(),
      ],
      {
        stdio: "inherit",
      },
    );
  });

program
  .command("build")
  .argument(
    "[args...]",
    `build to run: ${[...availableServices, "packages"].join(", ")} (default: all)`,
  )
  .description("run build command")
  .action(async (args: string[]) => {
    args = args.flatMap((arg) => arg.split(","));
    const packages = args.includes("packages");
    const projects = args.filter((arg) => arg !== "packages");
    if ((!projects.length && !packages) || packages) {
      await execa(
        "pnpm",
        [
          "turbo",
          "run",
          "build",
          "--filter",
          "'!./platforms/*'",
          "--filter",
          "'!./api'",
          "--filter",
          "'!./solana'",
          "--filter",
          "'!./ethereum'",
          "--filter",
          "'!./sui'",
        ],
        {
          stdio: "inherit",
          shell: true,
          cwd: projectRoot,
        },
      );
    }
    for (const platform of await fs.readdir("platforms")) {
      if (
        ((!projects.length && !packages) || projects.includes(platform)) &&
        (await fs
          .access(path.join(projectRoot, "platforms", platform, "package.json"))
          .then(
            () =>
              JSON.parse(
                fsSync.readFileSync(
                  path.join(projectRoot, "platforms", platform, "package.json"),
                  "utf8",
                ),
              ).scripts?.build,
          )
          .catch(() => false))
      ) {
        await execa("./mkpm", [`${platform}/build`], {
          stdio: "inherit",
          shell: true,
          cwd: projectRoot,
        });
      }
    }
    for (const service of availableServices) {
      if (
        ((!projects.length && !packages) || projects.includes(service)) &&
        (await fs
          .access(path.join(projectRoot, service, "package.json"))
          .then(
            () =>
              JSON.parse(
                fsSync.readFileSync(
                  path.join(projectRoot, service, "package.json"),
                  "utf8",
                ),
              ).scripts?.build,
          )
          .catch(() => false))
      ) {
        await execa("./mkpm", [`${service}/build`], {
          stdio: "inherit",
          shell: true,
          cwd: projectRoot,
        });
      }
    }
  });

program
  .command("test")
  .argument(
    "[args...]",
    `test to run: ${[...availableServices, "packages"].join(", ")} (default: all)`,
  )
  .description("run test command")
  .action(async (args: string[]) => {
    args = args.flatMap((arg) => arg.split(","));
    const packages = args.includes("packages");
    const projects = args.filter((arg) => arg !== "packages");
    if ((!projects.length && !packages) || packages) {
      await execa(
        "pnpm",
        [
          "turbo",
          "run",
          "test",
          "--filter",
          "'!./platforms/*'",
          "--filter",
          "'!./api'",
          "--filter",
          "'!./solana'",
          "--filter",
          "'!./ethereum'",
          "--filter",
          "'!./sui'",
        ],
        {
          stdio: "inherit",
          shell: true,
          cwd: projectRoot,
        },
      );
    }
    for (const platform of await fs.readdir("platforms")) {
      if (
        ((!projects.length && !packages) || projects.includes(platform)) &&
        (await fs
          .access(path.join(projectRoot, "platforms", platform, "package.json"))
          .then(
            () =>
              JSON.parse(
                fsSync.readFileSync(
                  path.join(projectRoot, "platforms", platform, "package.json"),
                  "utf8",
                ),
              ).scripts?.test,
          )
          .catch(() => false))
      ) {
        await execa("./mkpm", [`${platform}/test`], {
          stdio: "inherit",
          shell: true,
          cwd: projectRoot,
        });
      }
    }
    for (const service of availableServices) {
      if (
        ((!projects.length && !packages) || projects.includes(service)) &&
        (await fs
          .access(path.join(projectRoot, service, "package.json"))
          .then(
            () =>
              JSON.parse(
                fsSync.readFileSync(
                  path.join(projectRoot, service, "package.json"),
                  "utf8",
                ),
              ).scripts?.test,
          )
          .catch(() => false))
      ) {
        await execa("./mkpm", [`${service}/test`], {
          stdio: "inherit",
          shell: true,
          cwd: projectRoot,
        });
      }
    }
  });

program
  .command("check")
  .argument("[args...]", "checks to run: spelling, types, lint (default: all)")
  .description("run check command")
  .action(async (args: string[]) => {
    args = args.flatMap((arg) => arg.split(","));
    const spelling = !args.length || args.includes("spelling");
    const types = !args.length || args.includes("types");
    const lint = !args.length || args.includes("lint");
    let exitCode = 0;
    if (spelling) {
      try {
        await execa(
          "pnpm",
          [
            "cspell",
            "--unique",
            "`(git ls-files && (git lfs ls-files | cut -d' ' -f3))`",
          ],
          {
            stdio: "inherit",
            shell: true,
            cwd: projectRoot,
          },
        );
      } catch (err) {
        if (err instanceof Error && "exitCode" in err) {
          exitCode = (err as { exitCode: number }).exitCode;
        } else {
          exitCode = 1;
        }
      }
    }
    if (types) {
      try {
        await execa("turbo", ["run", "typecheck"], {
          stdio: "inherit",
          shell: true,
          cwd: projectRoot,
        });
      } catch (err) {
        if (err instanceof Error && "exitCode" in err) {
          exitCode = (err as { exitCode: number }).exitCode;
        } else {
          exitCode = 1;
        }
      }
    }
    if (lint) {
      try {
        await execa(
          "pnpm",
          [
            "biome",
            "check",
            "--fix",
            "--unsafe",
            "`(git ls-files && (git lfs ls-files | cut -d' ' -f3)) | sort | uniq -u | grep -E '(html)|(s?css)|(md)|(json)|(yaml)|([jt]sx?)$'`",
          ],
          {
            stdio: "inherit",
            shell: true,
            cwd: projectRoot,
          },
        );
      } catch (err) {
        if (err instanceof Error && "exitCode" in err) {
          exitCode = (err as { exitCode: number }).exitCode;
        } else {
          exitCode = 1;
        }
      }
    }
    process.exit(exitCode);
  });

program
  .command("count")
  .description("count lines of code")
  .action(async () => {
    await execa(
      "cloc",
      [
        "`(git ls-files && (git lfs ls-files | cut -d' ' -f3)) | sort | uniq -u | grep -E '(html)|(s?css)|(md)|(json)|(yaml)|([jt]sx?)$'`",
      ],
      {
        stdio: "inherit",
        shell: true,
        cwd: projectRoot,
      },
    );
  });

program
  .command("generate")
  .description("run generate command")
  .action(async () => {
    await execa("pnpm", ["turbo", "run", "generate"], {
      stdio: "inherit",
      shell: true,
      cwd: projectRoot,
    });
  });

program.parse(process.argv);

async function waitWithSpinner(
  services: string[],
  options: { interval?: number; timeout?: number } = {},
) {
  const { interval = 1000, timeout = 600000 } = options;
  const waitFunctions = {
    api: waitForApi,
    frappe: waitForFrappe,
    postgres: waitForPostgres,
    keycloak: waitForKeycloak,
  };
  const unreadyServices = [...services];
  const spinner = ora(
    `waiting for ${formatServiceList(unreadyServices)}`,
  ).start();
  function updateSpinner(readyService: string) {
    unreadyServices.splice(unreadyServices.indexOf(readyService), 1);
    spinner.succeed(`${readyService} is ready`);
    if (unreadyServices.length) {
      spinner.start(`waiting for ${formatServiceList(unreadyServices)}`);
    }
  }
  let timeoutId: NodeJS.Timeout | undefined;
  try {
    await Promise.race([
      Promise.all(
        services.map(async (service) => {
          const waitFn = waitFunctions[service];
          if (!waitFn) throw new Error(`Unknown service: ${service}`);
          await waitFn(interval);
          updateSpinner(service);
        }),
      ),
      new Promise((_, reject) => {
        timeoutId = setTimeout(() => reject(new Error("Timeout")), timeout);
      }),
    ]);
  } catch (err) {
    const error = err as Error;
    if (error.message === "Timeout") {
      spinner.fail(
        `${formatServiceList(unreadyServices)} timed out after ${timeout}ms`,
      );
    } else {
      spinner.fail(error.message);
    }
    throw error;
  } finally {
    clearTimeout(timeoutId);
  }
}
