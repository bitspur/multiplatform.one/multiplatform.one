/*
 * File: /src/logger/base.ts
 * Project: multiplatform.one
 * File Created: 21-01-2025 14:49:02
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import log from "loglevel";
import { platform } from "../platform/index";
import type { LogTransport } from "./transport/types";
import type { LoggerMetadata, LoggerOptions } from "./types";
import { LogLevel } from "./types";

export const defaultLoggerConfig: Omit<LoggerOptions, "metadata"> = {
  name: "default",
  type: "pretty",
  level:
    process.env.NODE_ENV === "development" ? LogLevel.Debug : LogLevel.Info,
} as const;

export abstract class BaseLogger {
  protected transport?: LogTransport;
  protected metadata: LoggerMetadata;
  protected logger: log.Logger;
  protected name: string;

  constructor(config: LoggerOptions | string = {}) {
    const { metadata, name, level } =
      typeof config === "string"
        ? { metadata: undefined, name: config, level: undefined }
        : config;
    this.metadata = metadata || {};
    this.name = name || "default";
    this.logger = name ? log.getLogger(name) : log;
    if (level !== undefined) this.logger.setLevel(level);
    this.transport = this.createTransport?.();
  }

  protected createTransport?(): LogTransport | undefined;

  public trace(...args: unknown[]): void {
    this.logger.trace(...args);
    this.sendToTransport(LogLevel.Trace, args);
  }

  public debug(...args: unknown[]): void {
    this.logger.debug(...args);
    this.sendToTransport(LogLevel.Debug, args);
  }

  public info(...args: unknown[]): void {
    this.logger.info(...args);
    this.sendToTransport(LogLevel.Info, args);
  }

  public warn(...args: unknown[]): void {
    this.logger.warn(...args);
    this.sendToTransport(LogLevel.Warn, args);
  }

  public error(...args: unknown[]): void {
    this.logger.error(...args);
    this.sendToTransport(LogLevel.Error, args);
  }

  public log(...args: unknown[]): void {
    this.logger.debug(...args);
    this.sendToTransport(LogLevel.Debug, args);
  }

  private sendToTransport(level: LogLevel, args: unknown[]): void {
    if (this.transport) {
      this.transport.send({
        level,
        message: args[0],
        args: args.slice(1),
        platform: platform.preciseName,
        timestamp: new Date().toISOString(),
      });
    }
  }

  public getSubLogger(input: LoggerOptions | string): BaseLogger {
    const subConfig =
      typeof input === "string"
        ? {
            name: input,
            metadata: this.metadata,
            level: this.logger.getLevel(),
          }
        : {
            ...input,
            metadata: { ...this.metadata, ...input.metadata },
          };
    return new (this.constructor as new (config: LoggerOptions) => BaseLogger)(
      subConfig,
    );
  }
}
