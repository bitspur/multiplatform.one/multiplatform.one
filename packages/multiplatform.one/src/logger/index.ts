/*
 * File: /src/logger/index.ts
 * Project: multiplatform.one
 * File Created: 21-01-2025 14:49:02
 * Author: Clay Risser
 * -----
 * BitSpur (c) Copyright 2021 - 2025
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { platform } from "../platform/index";
import { BaseLogger } from "./base";
import type { LoggerOptions } from "./types";

export class Logger extends BaseLogger {
  constructor(config: LoggerOptions | string = {}) {
    const metadata = {
      platform: platform.preciseName,
    };
    super({
      name: metadata.platform,
      ...(typeof config === "string" ? { name: config } : config),
      metadata,
    });
  }
}

export const logger = new Logger();

export * from "./types";
export * from "./base";
