# multiplatform.one example

> multiplatform.one example

- Electron - desktop apps
- Ethereum - smart contract platform
- Frappe - low code framework
- Keycloak - authentication
- OneStack - web and mobile frontend
- Prisma - database layer
- Solana - smart contract platform
- Sui - smart contract blockchain
- TypeGraphQL - graphql schema
- WebExt - browser extensions
- Yoga - graphql server

## Notes

- Do not directly log `ctx` to the console or the application will freeze. (e.g. `console.log(ctx)`).
