module sui::counter {
    public struct Counter has drop {
        value: u64
    }

    public fun create(): Counter {
        Counter {
            value: 0
        }
    }

    public fun increment(counter: &mut Counter) {
        counter.value = counter.value + 1;
    }

    public fun value(counter: &Counter): u64 {
        counter.value
    }
}
