import frappe
from frappe.tests.utils import FrappeTestCase


class TestQuestion(FrappeTestCase):
    def tearDown(self):
        """Clean up after tests"""
        frappe.db.sql("delete from `tabQuestion` where question like '_Test Question%'")
        frappe.db.commit()

    def test_create_question(self):
        """Test creation of a new question"""
        question = frappe.get_doc(
            {
                "doctype": "Question",
                "question": "_Test Question 1",
            }
        )
        question.insert()

        # Verify question was created
        self.assertTrue(question.name)
        self.assertEqual(question.question, "_Test Question 1")

        # Fetch from database and verify
        fetched_question = frappe.get_doc("Question", question.name)
        self.assertEqual(fetched_question.question, "_Test Question 1")

    def test_update_question(self):
        """Test updating an existing question"""
        question = frappe.get_doc(
            {
                "doctype": "Question",
                "question": "_Test Question 2",
            }
        ).insert()

        # Update the question
        question.question = "_Test Question 2 Updated"
        question.save()

        # Verify update
        updated_question = frappe.get_doc("Question", question.name)
        self.assertEqual(updated_question.question, "_Test Question 2 Updated")

    def test_delete_question(self):
        """Test deletion of a question"""
        question = frappe.get_doc(
            {
                "doctype": "Question",
                "question": "_Test Question 3",
            }
        ).insert()

        question_name = question.name
        question.delete()

        # Verify question was deleted
        with self.assertRaises(frappe.DoesNotExistError):
            frappe.get_doc("Question", question_name)

    def test_validate_required_fields(self):
        """Test validation of required fields"""
        # Try to create question without question text
        with self.assertRaises(frappe.ValidationError):
            frappe.get_doc({"doctype": "Question"}).insert()
