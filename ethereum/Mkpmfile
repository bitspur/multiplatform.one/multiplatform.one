# File: /Mkpmfile
# Project: @service/ethereum
# File Created: 14-01-2025 14:09:57
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2021 - 2025
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

include $(MKPM)/mkpm
include $(MKPM)/chain
DOTENV ?= $(PROJECT_ROOT)/.env
include $(MKPM)/dotenv

ACTIONS += deps
$(ACTION)/deps: $(PROJECT_ROOT)/package.json package.json
	@$(call make,$(PROJECT_ROOT)) \~deps DEPS_ARGS=$(DEPS_ARGS)
	@$(call done,$@)

ACTIONS += build~deps
BUILD_TARGETS := artifacts/artifacts.d.ts
artifacts/artifacts.d.ts:
	@$(call reset,build)
$(ACTION)/build: $(call git_deps,\.([mc]?[jt]sx?)$$)
	@$(PNPM) run build
	@$(call done,$@)

ACTIONS += test~build
$(ACTION)/test: $(call git_deps,\.([mc]?[jt]sx?)$$)
	@$(PNPM) run test
	@$(call done,$@)

.PHONY: dev +dev prelaunch
dev: ~deps +dev
+dev:
	@$(PNPM) run dev

.PHONY: localnet +localnet
localnet: ~deps +localnet
+localnet:
	@$(PNPM) run localnet

-include $(call chain)
